# -*- coding: utf-8 -*-
'''
   @file                    Lab0x01.py
   @brief                   This file provides our python code for the LED to run 3 different light patterns.
   @author                  Nolan Clapp and Parker Tenney
   @date                    April 12, 2021
'''

import pyb
import utime
import math

## @brief           Defines the location of the Button pin on the Nucleo
#  @details         This variable allows our code to recognize when an input
#                   comes from the button located at C13
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)

## @brief           Defines the location of the LED pin on the Nucleo
#  @details         This variable allows us to change the brightness and pattern
#                   of the LED located at pin A5
pinA5 = pyb.Pin (pyb.Pin.cpu.A5)

## @brief           Uses Timer 2 in pyb at a set freqency
#  @details         This variable allows us to chagne the onboard timer on the 
#                   Nucleo, as well as the frequency it runs at
tim2 = pyb.Timer(2, freq = 20000)

## @brief           Defines the channel on the Nucleo that the timer will be using
#  @details         This variable allows us to change the channel that the timer
#                   is running through, as well as the location that we want it to.
#                   For example, our code will be running on channel 1, using 
#                   the variable tim2 to set the onboard timer as well as frequency,
#                   and sets the pin location to A5 so that this function works 
#                   on our LED
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)

## @brief           Varaible that changes when the button is pressed
#  @details         When the button is pressed, buttonpress value is set to True
#                   it is set to false after the program transitions states
buttonpress=False 



def onButtonPressFCN(IRQ_src):
    '''
    @brief          Changes the Button Pressed value to True.
    @details        This function takes the IRQ_src input and allows us to translate 
                    a button push to set a variable value to true, so we can use the 
                    button to transition from one state to another, that is, the function
                    will run when the button is pressed.
    @param IRQ_src    Variable that determines when the button is pressed. 

    @return         This function returns a value of "True" for the variable 
                    "buttonpress" when the button on the board is pressed. 
    '''
    global buttonpress
    buttonpress=True
    
def TimeReset():
    '''
    @brief		Resets the timer

    @details		This program takes the timestart variable and sets it to the time passed in order to obtain a net 0 time through the timeupdate function.

    @return		This function returns the value “timestart” which will be equal to the time passed.
    '''
    ##
    #@ brief    Variable for the start time
    #@ details  This variable is set to the number of ticks when the program begins
    #           We set this variable to zero later on, so that our elapsed time is 
    #           equal to the time that the program stops. Other functions update this variable to 
    #           change the value of TimeStart, so that we get the desired output. 
    TimeStart = utime.ticks_ms()
    return TimeStart
    

def TimeUpdate(TimeStart):
    '''
    @brief          Updates the variable TotalTime.
    @details        This function takes the input of TimeStart, and sets a 
                    variable TimeStop (the number of ticks that has passed while 
                    the progratm is running) and then takes the difference between
                    the two to determine the elapsed time. 
    @param TimeStart    Variable that is set to the number of ticks when the program
                        is first run

    @return         This function retruns the TotalTime elapsed that the program has been running 
    '''
    ##
    #@ brief    Variable for the stopping time
    #@ details  This variable uses the utime.ticks_ms() function to set
    #           its value to the number of ticks that the progam has been 
    #           running (independant TimeStart)
    TimeStop = utime.ticks_ms()
    
    ##
    #@ brief    Variable for the elapsed program time
    #@ details  This variable uses the utime.ticks_diff() function to determine
    #           the difference between TimeStart and TimeStop in ticks 

    TotalTime = utime.ticks_diff(TimeStop, TimeStart)
    return TotalTime    


def SQUARE(TotalTime):
    '''
    @brief		Outputs a square wave
    @details		This program creates a square wave by comparing the totaltime value to .5, and returning either a 0 or 100 respectively.
    @param	  totaltime variable that is the elapsed program time.
    @return		This function returns a 100 if the totaltime is greater than .5, and a 0 if it is less than
    '''
    return 100 * ((TotalTime+.5) % 1 > .5)

def SINE(TotalTime):
    '''
    @brief		Outputs a sine wave
    @Details		This program creates a sine wave by computing the sine of the 
                totaltime, and shifting it up in order to return a positive number.
    @param	  totaltime variable that is the elapsed program time.
    @return		This function returns the sine of the totaltime variable all positive on a range from 0 to 100.
    '''
    return 50 *(math.sin(TotalTime)+1)

def SAWTOOTH(TotalTime):
    '''
    @brief		Outputs a sawtooth wave
    @details		This program creates a sawtooth wave by modding the totaltime with 1, and multiplying it by 100.
    @param	  totaltime variable that is the elapsed program time.
    @return		This function returns the mod of totaltime with 1, and multiplies by 100.
    '''
    return 100*(TotalTime%1.0)

##
#@ brief Variable for external interupt
#@ details  This variable is for associating the callback function with the pin by setting 
#           up an exetrnal interupt
ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)   


TimeStart=0

##
#@ brief Variable for initial LED brightness
#@ details  This variable sets our initial LED brightness to 0 and will later
#           be over written to have different values, depending on the time, 
#           and the LED pattern we are running. 
brightness=0


if __name__ == '__main__':
    
    ##
    #@ brief Variable to set our inital state to 0
    #@ details  This variable determines where in our Loop we begin. By setting
    #           its intitial value to 0, we can be sure our program runs sequentially 
    #           the first time through. It will be changed in our program so that when
    #           the button is pressed, the state is updated to follow the FSM
    state=0
    
    while(True):
        try:
            # State zero is the initializing state. 
            if (state==0):
                print ('Press Buttong B1 to start and press again to cycle through LED patterns')
                state=1
            # Once it is dune initializing, the user is prompted with a statment telling them how the program works, 
            # and when they press the button, we transition to state 2, if not, the program will keep the prompt on the
            # screen until they press the button
            
            # Similarly, after the button is pressed, we move into State 2, where the LED brightness mimics a Square Wave, 
            # if the button is not pressed, we will remain in this state until the button is pressed. 
            elif (state==1):
                if (buttonpress==True):
                    state=2
                    buttonpress=False
                    TimeStart=TimeReset()
                    print('Square wave pattern selected')
                else:
                    state=1
            # After the button is pressed, we move into State 3, where the LED brightness mimics a Sinewave, 
            # if the button is not pressed, we will remain in this state until the button is pressed.        
            elif (state==2):
                
                 if (buttonpress==True):
                     state=3
                     buttonpress=False
                     TimeStart=TimeReset()
                     print('Sinewave Pattern Selected')
                 else: 
                    
                    TotalTime = TimeUpdate(TimeStart)
                    brightness = SQUARE(TotalTime/1000)
                    t2ch1.pulse_width_percent(brightness)
                    state=2
             # After the button is pressed, we move into State 4, where the LED brightness mimics a Sawtooth Wave, 
             # if the button is not pressed, we will remain in this state until the button is pressed.  
            elif (state==3):

                 if (buttonpress==True):
                     state=4       
                     buttonpress=False
                     TimeStart=TimeReset()
                     print('Sawtooth wave selected')
                 else: 
                     TotalTime = TimeUpdate(TimeStart)
                     brightness = SINE(TotalTime/(1591.6))
                     #print(brightness)
                     t2ch1.pulse_width_percent(brightness)
                     state=3
            # After the button is pressed, we move back into state 2, creating a loop if the button is pressed.  
            # if the button is not pressed, we will remain in this state until the button is pressed.            
            elif (state==4):
                 
                 if (buttonpress==True):
                     state=2
                     buttonpress=False
                     TimeStart=TimeReset()
                     print('Square wave pattern selected')
                 else: 
                     TotalTime = TimeUpdate(TimeStart)
                     brightness = SAWTOOTH(TotalTime/1000)
                     t2ch1.pulse_width_percent(brightness)                     
                     state=4     
        # This loop will continue until the user types a keyboard interupt, which will end the program and display "Program Ended"             
        except KeyboardInterrupt:
            print ('Program Ended')
            break
                      

                         
                    