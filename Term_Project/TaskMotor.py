"""
   @file                    FTerm_TaskMotor.py
   @brief                   This file provides our main code to run our motor driver to power the motors
   @author                  Nolan Clapp and Parker Tenney
   @date                    December 8, 2021
"""
import utime
import MotorDriver
import time
class TaskMotor:
    
    def __init__(self,period,MotorObj,motordrive,MotorNum,measuredx,measuredy,xpos,ypos,zpos,Balance,km,kp):
        '''
        @brief      Initializes the Motor task with required shares
        @details    Allows necessary parameters to be passed into task motor
        '''
        ## @brief           Defines the period of the encoder 
        #  @details         Defines the period of the encoder as the largest 16 bit 
        #                   number, 65,535
        self.period=period
        
        ## @brief           Defines the count used to run the task
        #  @details         Uses onboard Utime timer to create a count to be 
        #                   sure our task is running on time
        self.count=utime.ticks_us()
        ## @brief           Allows us to choose which motor to run
        #  @details         We can input the motor number (1 or 2) for the motor 
        #                   we want to run
        self.MotorNum = MotorNum
        ## @brief           This creates an object from motor driver for each motor
        #  @details         This variable creates an object from the motor driver,
        #                   which will allow us to utilize multiple motors without
        #                   creating multiple files
        self.MotorObj=MotorObj
        ## @brief           Defines the duty cycle for the motor
        #  @details         This variable allows us to input a percent (0-100)
        #                   for the duty cycle we want the motor to run at
        
        ## @brief           Creates an object for our motor driver
        #  @details         This variable takes in the timer number, to fully
        #                   define our motor driver
        self.motordrive=motordrive
        ## @brief           Enables the motor driver
        #  @details         This variable enables the motor driver so that we can 
        #                   actually run the motor for a given duty cycle
        self.motordrive.enable()
        ## @brief           Variable for the measured angular velocity in the x plane
        #  @details         This variable allows us to share x plane angular velocity between files
        self.measuredx=measuredx
        ## @brief           Variable for the measured angular velocity in the y plane
        #  @details         This variable allows us to share y plane angular velocity between files
        self.measuredy=measuredy
        ## @brief           Variable that toggles balancing the ball
        #  @details         This variable allows us to share a True False that will control
        #                   whether or not the board tries to balance the ball
        self.Balance = Balance
        ## @brief           Variable for the current time
        #  @details         This variable allows us to count using the onboard timer
        self.count=utime.ticks_us()
        ## @brief           This variable sets the state to 0
        #  @details         Allows us to transition to state 0
        self.state=0
        ## @brief           This variable sets initial a value to zero
        #  @details         Allows calculate error*kp to produce the desuired closed loop response
        self.a=0
        ## @brief           This variable sets initial x angle to zero
        #  @details         Allows us to intiially balance the board to zero on the x plane
        self.xang = 0
        ## @brief           This variable sets initial x angle to zero
        #  @details         Allows us to intiially balance the board to zero on the x plane
        self.yang = 0
        ## @brief           This shared variable sets the speed control gain
        #  @details         Allows us to set and read a shared variable for the gain that 
        #                   adjusts the level of speed control.
        self.kp=kp
        ## @brief           This shared variable sets the position control gain
        #  @details         Allows us to set and read a shared variable for the gain that 
        #                   adjusts the level of speed control.
        self.km=km
        ## @brief           Shared variable for x position on the touch panel
        #  @details         This variale allows us to share the x position read from 
        #                   the touch panel, but only when the panel is being touched
        self.xpos = xpos
        ## @brief           Shared variable for y position on the touch panel
        #  @details         This variale allows us to share the y position read from 
        #                   the touch panel, but only when the panel is being touched
        self.ypos = ypos
        ## @brief           Shared variable for z position on the touch panel
        #  @details         This variale allows us to share the verify when the panel is being touched
        self.zpos = zpos
        ## @brief This variable allows us to calculate the x velocity of the ball easily
        self.oldxpos=0
        ## @brief This variable allows us to calculate the y velocity of the ball easily
        self.oldypos=0
        ## @brief This variable allows us to verify the angular velocity about the x axis with the IMU angular velo values
        self.xangle = 6
        ## @brief This variable allows us to verify the angular velocity about the y axis with the IMU angular velo values
        self.yangle = 6
        ## @brief This variable allows us to set the max angle that our board will move to
        # @details This allows us to stop our motors and code to prevent damage if the board
        #           reaches an angle greater than 25 degrees
        self.death = 25
    def run(self):
        ''' @brief              Runs the functions in TaskMotor
            @details            This function is responsible for running the functions 
                                in Motor Task and actually allows the motors to run
        '''
        # Thios code ensures that the task runs on time
        if self.count>=self.period:
            self.count+=self.period
        
            if self.state==0:
                self.state=1
            #reads balance to determine when the user wants to balance the ball
            elif self.state==1:
                if self.Balance.read() == True:
                    self.state=2
                
                    
                    
             #ABOUT Y AXIS       
            elif self.state==2:
                
                if self.zpos.read() == False:
                    self.state=2
                # if there is a touch, move to run xpos and ypos
                elif self.zpos.read() == True:
                    
                    self.xposition = self.xpos.read()
                    self.xang = (-1*self.xposition/80)*self.xangle-1.3
                
                self.measured1 = self.measuredy.read()       #WRITE EULER TO THIS VAR
                # Closed loop controller stuff
                if self.measured1>self.xang:
                    e = self.xang- self.measured1
                    #wont work above max set angle 
                    if self.measured1>self.death:
                        self.MotorObj.set_duty(0)
                    else:
                        # Closed loop controller stuff
                        self.a=e
                        self.xspeed = self.xpos.read() - self.oldxpos
                        self.oldxpos = self.xpos.read()
                        #duty setting using scaling factors and gain values
                        self.newduty = ((self.km.read()*abs(self.a)+self.kp.read()*abs(self.xspeed))**.25)*5.8
                        if abs(self.newduty)<100 and abs(self.xspeed) < 3:
                            self.MotorObj.set_duty(self.newduty*2)
                    
                elif self.measured1<self.xang:
                    e = self.xang- self.measured1
                    # turns off if the angle is greater than 'death'
                    if self.measured1<-self.death:
                        self.MotorObj.set_duty(0)
                    else:
                         # Closed loop controller stuff
                         self.a=e
                         self.xspeed = self.xpos.read() - self.oldxpos
                         self.oldxpos = self.xpos.read()
                         #duty setting using scaling factors and gain values
                         self.newduty = ((self.km.read()*abs(self.a) + self.kp.read()*(abs(self.xspeed)))**.25)*5.8
                         if abs(self.newduty)<100 and abs(self.xspeed) < 3:
                             self.MotorObj.set_duty(-self.newduty*2)
                         
                         
                         #print(self.measured1)
                # transitions to motor 2
                self.MotorNum.write(False)
                self.state=3
            
            
            
            
            #ABOUT X AXIS
            elif self.state==3:
                #print(self.ypos.read())
                if self.zpos.read() == False:
                    self.state=2
                # if theres a touch, do something
                elif self.zpos.read() == True:
                    self.yposition = self.ypos.read()
                    self.yang = (-1*self.yposition/40)*self.yangle
                
                self.measured2 = self.measuredx.read()       #WRITE EULER TO THIS VAR
                if self.measured2 > self.yang:
                    # Closed loop controller stuff
                    e2 = self.yang - self.measured2
                    if self.measured2 > self.death:
                        self.MotorObj.set_duty(0)
                    else:
                        self.a2=e2
                        self.yspeed = self.ypos.read() - self.oldypos
                        self.oldypos = self.ypos.read()
                        #duty setting using scaling factors and gain values
                        self.newduty2 = ((self.km.read()*abs(self.a2) + self.kp.read()*(1.5*abs(self.yspeed)))**.25)*6
                        
                        if abs(self.newduty2)<100 and abs(self.yspeed) < 3:
                            self.MotorObj.set_duty(-self.newduty2*2)
                            #print(-self.newduty2*2)
                        #print(self.measured2)
                        
                        
                elif self.measured2 < self.yang:
                    e3 = self.yang - self.measuredx.read()
                    if self.measured2 < -self.death:
                        self.MotorObj.set_duty(0)
                    else:
                        #print(self.measured2)
                        self.a3=e3
                        self.yspeed = self.ypos.read() - self.oldypos
                        self.oldypos = self.ypos.read()
                        #duty setting using scaling factors and gain values
                        self.newduty2 = ((self.km.read()*abs(self.a3) + self.kp.read()*(1.5*abs(self.yspeed)))**.25)*6
                        
                        if abs(self.newduty2)<100 and abs(self.yspeed) < 3:
                            self.MotorObj.set_duty(self.newduty2*2)
                            #print(self.newduty2*2)
                #back to state 1 to check timing 
                self.MotorNum.write(True)
                self.state=1
                
                    

           
        