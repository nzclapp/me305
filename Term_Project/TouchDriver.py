"""
   @file                    FTerm_TouchDriver.py
   @brief                   This file provided the file to interface with the resistive touch panel
   @details                 This file allows us to gather touch data from the RTP
   @author                  Nolan Clapp and Parker Tenney
   @date                    December 8, 2021
"""

import pyb
import shares
import time
from ulab import numpy as np

class TouchDriver:
    ''' @brief A TouchDriver class to interface with the RTP
    '''    

    def __init__(self,xpos,ypos,zpos):
        ''' @brief              Initializes the touch driver and necessary shares
            @details            Sets the starting values for our touch driver and defines the shares
            @param period       The period, in microseconds, between runs of 
                                the task.
            @param Data         Share object that turns the data collection on or off
            @param xpos         The current x position
            @param ypos         The current y position
            @param measuredy    The current x angle
            @param measuredx    The current y angle
        '''   
        ## @brief           Shared variable for x position on the touch panel
        #  @details         This variale allows us to share the x position read from 
        #                   the touch panel, but only when the panel is being touched
        #                   In this document, several xpos variables are called
        #                   but are all just instances of this variable at different values
        self.xpos=xpos
        ## @brief           Shared variable for y position on the touch panel
        #  @details         This variale allows us to share the y position read from 
        #                   the touch panel, but only when the panel is being touched
        #                   In this document, several ypos variables are called
        #                   but are all just instances of this variable at differnt values
        self.ypos=ypos
        ## @brief           Shared variable for whether or not the panel is being touched
        #  @details         This variale allows us to share whether a touch is being sensed
        #                   by the panel or if there is no touch on the touch pad.
        self.zpos=zpos
        
    def xscan(self):
        ''' @brief Scans the x-axis for a touch
        '''  
        ## @brief          Variable for the negative x side of the RTP
        #  @details         Allows us to find the x position of touch
        pinxm = pyb.Pin(pyb.Pin.cpu.A1, pyb.Pin.OUT_PP)
        ## @brief          Variable for the negative x side of the RTP
        #  @details         Allows us to find the x position of touch
        pinxp = pyb.Pin(pyb.Pin.cpu.A7, pyb.Pin.OUT_PP)
        pinxm=pinxm.low()
        pinxp=pinxp.high()
        ## @brief          Variable for the positive y side of the RTP
        #  @details         Allows us to find the x position of touch
        pinyp=pyb.Pin(pyb.Pin.cpu.A6, pyb.Pin.IN)
        ## @brief          Variable for the x ADC value
        adcx=pyb.ADC(pyb.Pin.cpu.A0)
        xposition=adcx.read()*176/4095-88
        self.xpos.write(xposition)

    
    def yscan(self):
        ''' @brief Scans the y-axis for a touch
        '''
        ## @brief          Variable for the negative y side of the RTP
        #  @details         Allows us to find the y position of touch
        pinym=pyb.Pin(pyb.Pin.cpu.A0, pyb.Pin.OUT_PP)
        ## @brief          Variable for the positive y side of the RTP
        #  @details         Allows us to find the y position of touch
        pinyp=pyb.Pin(pyb.Pin.cpu.A6, pyb.Pin.OUT_PP)
        pinym=pinym.low()
        pinyp=pinyp.high()  
        ## @brief          Variable for the positive x side of the RTP
        #  @details         Allows us to find the y position of touch
        pinxp=pyb.Pin(pyb.Pin.cpu.A7, pyb.Pin.IN)
        adcy=pyb.ADC(pyb.Pin.cpu.A1)
        yposition=adcy.read()*100/4095-50
        self.ypos.write(yposition)
 
    
    def zscan(self):
        ''' @brief Scans the z-axis for a touch
            @details If there is a touch registerd on the panel, write zpos=True
        '''
        pinym = pyb.Pin(pyb.Pin.cpu.A0, pyb.Pin.OUT_PP)
        pinxp = pyb.Pin(pyb.Pin.cpu.A7, pyb.Pin.OUT_PP)
        pinym=pinym.low()
        pinxp=pinxp.high()
        adcz=pyb.ADC(pyb.Pin.cpu.A1)
        ## @brief          Variable for z touch
        #  @details         Allows us to find the if there is a touch on the RTP
        z=adcz.read()
        if z>=4050:
            self.zpos.write(False)
        else:
            self.zpos.write(True)
        
    def calibrate(self):
        ''' @brief Code to calibrate the touch panel
            @details Code was not fully implemented becasue the ball would balance 
            sufficiently without running the calibration
        '''
        state=0
        
        # Prompts the user to touch multiple points in order to calibrate the touch panel
        
        print('Touch (0,0)')
        while state==0:
            TouchDriver.zscan(self)
            if self.zpos.read()==True:
                xpos1=self.xpos.read()
                ypos1=self.ypos.read()
                time.sleep(1)
                print('Touch (40,0)')
                state=1
                
        while state==1:
            TouchDriver.zscan(self)
            if self.zpos.read()==True:
                xpos2=self.xpos.read()
                ypos2=self.ypos.read()
                time.sleep(1)
                print('Touch (-40,0)')
                state=3
                
        while state==2:
            TouchDriver.zscan(self)
            if self.zpos.read()==True:
                xpos3=self.xpos.read()
                ypos3=self.ypos.read()
                time.sleep(1)
                print('Touch (0,40)')
                state=3
                
        while state==3:
            TouchDriver.zscan(self)
            if self.zpos.read()==True:
                xpos4=self.xpos.read()
                ypos4=self.ypos.read()
                time.sleep(1)
                print('Touch (0,-40)')
                state=4
                
        while state==4:
            TouchDriver.zscan(self)
            if self.zpos.read()==True:
                xpos5=self.xpos.read()
                ypos5=self.ypos.read()
                time.sleep(1)
                print('Touch (40,40)')
                state=5
                
        while state==5:
            TouchDriver.zscan(self)
            if self.zpos.read()==True:
                xpos6=self.xpos.read()
                ypos6=self.ypos.read()
                time.sleep(1)
                print('Touch (80,-40)')
                state=6
                              
        while state==6:
            TouchDriver.zscan(self)
            if self.zpos.read()==True:
                xpos7=self.xpos.read()
                ypos7=self.ypos.read()
                time.sleep(1)
                print('Calibration Complete')
                state=7
                               
        if state==7:
            ## @brief          pos matrix that is the readings resulting from the user prompted touch
            pos=np.array([[xpos1,ypos1,1],[xpos2,ypos2,1],[xpos3,ypos3,1],[xpos4,ypos4,1],[xpos5,ypos5,1],[xpos6,ypos6,1],[xpos7,ypos7,1]])
            ## @brief          pos2 matrix that is the positions prompted for the user to touch
            pos2 = np.array([[0, 0], [40, 0], [-40, 0], [0, 40], [0, -40], [40, 40], [80, -40]])
            ## @brief          A matrix that allows us to do matrix math easier
            A=np.dot(pos.transpose(),pos)
            ## @brief          B matrix that allows us to do matrix math easier
            B=np.linalg.inv(A)
            ## @brief          C matrix that allows us to do matrix math easier
            C=np.dot(pos.transpose(),pos2)
            ## @brief          D matrix that allows us to do matrix math easier
            self.D=np.dot(B,C)
            return (self.D[0, 0], self.D[1, 0], self.D[0, 1], self.D[1, 1], self.D[2, 0], self.D[2, 1])
    