"""
   @file                    FTern_InterfaceTask.py
   @brief                   This file provides our main code to interact with the user inputs
   @author                  Nolan Clapp and Parker Tenney
   @date                    December 4, 2021
"""
import pyb
import utime
import time



class InterfaceTask:
    ''' @brief      Interface with the user
        @details    Interfaces with the user to gather keyboard inputs that
                    control the program
    '''
    
    def __init__(self, period, Calib, km, kp, Balance, Data):
        ''' @brief              Constructs the interface task.
            @details            The interface task is implemented as a finite state
                                machine.
            @param period       The period, in microseconds, between runs of 
                                the task.
            @param Calib        Share object that turns the IMU calibration on or off
            @param km           The position control gain
            @param kp           The speed control gain
            @param Balance      Share object that turns the board balance on or off
            @param Data         Share object that turns the data collection on or off
        '''     
        ## @brief           The initial state of the FSM
        #  @details         This variable allows us to set the initial state to 0
        self.S0_INIT = 0
        ## @brief           The first state of the FSM
        #  @details         This variable allows us to set the state to 1
        self.S1_WAIT_FOR_INPUT = 1
        ## @brief           The second state of the FSM
        #  @details         This variable allows us to set the state to 2
        self.S2_CALIB = 2
        ## @brief           The third state of the FSM
        #  @details         This variable allows us to set the state to 3
        self.S3_GAIN = 3
        ## @brief           The fourth state of the FSM
        #  @details         This variable allows us to set the state to 4
        self.S4_BALANCE = 4
        ## @brief           The fifth state of the FSM
        #  @details         This variable allows us to set the state to 5
        self.S5_PRINT = 5
        ## @brief           The sixth state of the FSM
        #  @details         This variable allows us to set the state to 6
        self.S6_STOP = 6
        ## @brief           The variable for the period
        #  @details         This variable allows us control what period the program runs
        self.period = period
        ## @brief           Shared Variable for Encoder Delta
        #  @details         This variable allows us to share the encoder delta
        #                   between task user and task encoder
        self.Calib = Calib
        ## @brief           This shared variable sets the position control gain
        #  @details         Allows us to set and read a shared variable for the gain that 
        #                   adjusts the level of position control.
        self.km = km
        ## @brief           This shared variable sets the speed control gain
        #  @details         Allows us to set and read a shared variable for the gain that 
        #                   adjusts the level of speed control.
        self.kp = kp
        ## @brief           Shared variable for balancing the ball
        #  @details         This variale allows us to use choose whether or not we want to balance the ball
        self.Balance = Balance
        ## @brief           Shared variable for Data Collection
        #  @details         This variale allows us to use choose whether or not we want to collect data
        self.Data = Data
        ## @brief           Defines a time variable for next time
        #  @details         This variable allows us to compare time to next time,
        #                   and fall into the FSM
        self.NextTime = utime.ticks_add(utime.ticks_us(), self.period)
        ## @brief           Sets the initial state to zero
        #  @details         This variable allows us to move into the zero state of the FSM
        self.state = self.S0_INIT
        ## @brief           Interface with the keyboard
        #  @details         Allows us to take keyboard inputs, and use them in our
        #                   code as values.
        self.ser_port = pyb.USB_VCP()
        ## @brief           This shared variable sets the position control gain as a string we can edit
        #  @details         Allows us to set and read a shared variable for the speed control gain that 
        #                   we can edit with user input keystrokes
        self.km_str = ""
        ## @brief           This shared variable sets the position control gain as a string we can edit
        #  @details         Allows us to set and read a shared variable for the speed control gain that 
        #                   we can edit with user input keystrokes
        self.kp_str = ""
        
    def run(self):
        ''' @brief          Runs the user inteface task.
            @details        The interface task is implemented as a finite state
                            machine.
        '''     
        ## @brief           Counts the time
        #  @details         Variable that counts the total time elapsed
        self.time = utime.ticks_us()
        
        if (utime.ticks_diff(self.time, self.NextTime) >= 0):   # Only runs on the correct period
            
                if self.state == self.S0_INIT:   # User interface shown to the User
                    print('Press these buttons below to use our software:')
                    print('c: Calibrate the imu (do this the first run)')
                    print('k: adjust the gain values')
                    print('b: balance the board')
                    print('d: stop balancing the board')
                    print('p: Print out the required data')
                    
                    self.state = self.S1_WAIT_FOR_INPUT
                    
                    
                if self.state == self.S1_WAIT_FOR_INPUT:   # Takes the user input keystroke and moves to designated state
                    
                    if self.ser_port.any():
                        ## @brief           Variable that is the user input
                        #  @details         Variable that is defined as the keyboard stroke
                        #                   pressed by the user
                        self.userinput = self.ser_port.read(1)
                        
                        if(self.userinput == b'c'):
                            self.Calib.write(True)
                            self.state = self.S2_CALIB
                            
                        elif(self.userinput == b'k'):
                            ## @brief           Variable that lets us set the state for input
                            #  @details         Variable that allows us to have seperate states for gain
                            #                   editing with user inputs
                            self.input_state = 1
                            print('enter gain for motor')
                            self.state = self.S3_GAIN
                            
                        elif(self.userinput == b'b'):
                            self.state = self.S4_BALANCE                            

                        elif(self.userinput == b'p'):
                            self.state = self.S5_PRINT
                             
                        elif(self.userinput == b'd'):
                            self.state = self.S6_STOP
                    
                    
                elif self.state == self.S2_CALIB:   # Sets the IMU calibration to run
                    if self.Calib.read == True:
                        self.state == self.S2_CALIB
                    else:
                        self.state = self.S1_WAIT_FOR_INPUT
                    
                    
                elif self.state == self.S3_GAIN:  # Allows user to edit the gain of the speed and position control
                    
                    if self.input_state == 1:  # Adjust position control gain
                        
                        if self.ser_port.any():
                            
                            self.char_in2 = self.ser_port.read(1).decode()
                            
                            if self.char_in2.isdigit():
                                self.km_str += self.char_in2
                                print(self.km_str)
                                self.input_state = 1
                                
                            elif self.char_in2 == '-':
                                if self.km_str == "":
                                    self.km_str += self.char_in2
                                    print(self.km_str)
                                    self.input_state = 1
                                else:
                                    self.input_state = 1
                            elif self.char_in2 =='.':
                                if "." in self.km_str:
                                    self.input_state = 1
                                else:
                                    self.km_str += self.char_in2
                                    print(self.km_str)
                            elif self.char_in2 =='\x7F':
                                self.km_str = self.km_str[:-1]
                                print(self.km_str)
                                    
                            elif self.char_in2 == '\r' or self.char_in2 == '\n':
                                self.input_state = 2
                                
                                
                    elif self.input_state == 2: 
                        print('Enter Speed Gain Value')
                        ## @brief           Variable that is the float of the postion gain string
                        #  @details         Variable that is defines as the numeric conversion of the position gain string
                        self.km_number = float(self.km_str)
                        print(self.km_number)
                        self.km.write(self.km_number)
                        self.input_state = 3
                        
                                
                    elif self.input_state == 3: # Adjust position control gain
                        
                        if self.ser_port.any():
                            self.char_in3 = self.ser_port.read(1).decode()
                                
                            if self.char_in3.isdigit():
                                self.kp_str += self.char_in3
                                print(self.kp_str)
                                self.input_state2 = 3
                                
                            elif self.char_in3 == '-':
                                if self.kp_str == "":
                                    self.kp_str += self.char_in3
                                    print(self.kp_str)
                                    self.input_state2 = 3
                                else:
                                    self.input_state2 = 3
                            elif self.char_in3 =='.':
                                if "." in self.kp_str:
                                    self.input_state2 = 3
                                else:
                                    self.kp_str += self.char_in3
                                    print(self.kp_str)
                                    
                            elif self.char_in3 =='\x7F':
                                
                                self.kp_str = self.kp_str[:-1]
                                print(self.kp_str)
                                    
                            elif self.char_in3 == '\r' or self.char_in3 == '\n':
                                self.input_state = 4
                                    
                    elif self.input_state == 4:
                        ## @brief           Variable that is the float of the speed gain string
                        #  @details         Variable that is defines as the numeric conversion of the speed gain string
                        self.kp_number = float(self.kp_str)
                        print(self.kp_number)
                        self.kp.write(self.kp_number)
                        self.oldtime= self.time
                        self.state = self.S1_WAIT_FOR_INPUT
                        
                    
                elif self.state == self.S4_BALANCE:  # Sets the ball balance to run
                    self.Balance.write(True)
                    self.state = self.S1_WAIT_FOR_INPUT
                    
                    
                elif self.state == self.S5_PRINT:   # Sets the data collection to run
                    self.Data.write(True)
                    self.state = self.S1_WAIT_FOR_INPUT
                    
                    
                elif self.state == self.S6_STOP:  # Stops the board from balancing, and moving entirely
                    self.Balance.write(False)
                    self.state = self.S1_WAIT_FOR_INPUT
                    