"""
   @file                    FTerm_TaskTouch.py
   @brief                   This file provided the file to run the RTP driver code
   @details                 This file allows us to gather touch data from the RTP
   @author                  Nolan Clapp and Parker Tenney
   @date                    December 8, 2021
"""
import TouchDriver
import utime
import os
## @brief This is a variable that defines the touch_cal_coeff file name
filename="Touch_Coeff.txt"

class TaskTouch:
    ''' @brief A TaskTouch class to interface with the RTP driver and collect data
    '''    
    def __init__(self, period,xpos,ypos,zpos,CalibTouch):
        ''' @brief              Initializes the Task touch and necessary shares
            @details            Sets the starting values for our task touch and defines the shares
            @param period       The period, in microseconds, between runs of 
                                the task.
            @param xpos         The current x position
            @param ypos         The current y position
            @param zpos         Shares whether or not a touch is detected
            @param CalibTouch   Object that determines if the touch calibration needs to be run
        ''' 
        ## @brief           Defines the period of TaskTouch 
        #  @details         Defines the period of the task passed in through init
        self.period=period
        ## @brief           Shared variable for x position on the touch panel
        #  @details         This variale allows us to share the x position read from 
        #                   the touch panel, but only when the panel is being touched
        #                   In this document, several xpos variables are called
        #                   but are all just instances of this variable at different values
        self.xpos=xpos
        ## @brief           Shared variable for y position on the touch panel
        #  @details         This variale allows us to share the y position read from 
        #                   the touch panel, but only when the panel is being touched
        #                   In this document, several ypos variables are called
        #                   but are all just instances of this variable at differnt values
        self.ypos=ypos
        ## @brief           Shared variable for whether or not the panel is being touched
        #  @details         This variale allows us to share whether a touch is being sensed
        #                   by the panel or if there is no touch on the touch pad.
        self.zpos=zpos
        ## @brief           Shared variable to determine when we want to calibrate RTP
        self.CalibTouch=CalibTouch
        
    def run(self):
        ''' @brief              Runs the functions in TaskTouch
            @details            This function is responsible for running the functions 
                                in TouchDriver and allows us to collect data
        '''
        if self.CalibTouch.read()==True:  # Determines if the calibration of touch is set to run, and will start calibration
            if filename in os.listdir():         # Looks for file with previous calibration constants        
                with open(filename, 'r') as f:
                    cal_data_string = f.readline()
                    ## @brief List of calibration constants and center values calculated during calibration step
                    # @details Kxx, Kxy, Kyx, Kyy, Xc, Yc are all stored in this data string
                    cal_values = [float(cal_value) for cal_value in cal_data_string.strip().split(',')]
                    self.Kxx = cal_values[0]
                    self.Kxy = cal_values[1]
                    self.Kyx = cal_values[2]
                    self.Kyy = cal_values[3]
                    self.Xc  = cal_values[4]
                    self.Yc  = cal_values[5]
                    self.CalibTouch.write(False)
                    
            else:   # If no file, it will write the calibration constants to it  
                with open(filename, 'w') as f:
                    (Kxx, Kxy, Kyx, Kyy, Xc, Yc) = TouchDriver.TouchDriver.calibrate(self)
                    f.write(f"{Kxx}, {Kxy}, {Kyx}, {Kyy}, {Xc}, {Yc}\r\n")
                    self.CalibTouch.write(False)
        else:  # after calibration the touch panel now needs to update as seen below
            
            TouchDriver.TouchDriver.zscan(self)     # Checks to se if touch panel senses a touch
           
            if self.zpos.read()==True:  # If a touch is sensed it will update the x pos anf ypos
                
                TouchDriver.TouchDriver.xscan(self)
                TouchDriver.TouchDriver.yscan(self)
                ## @details Variable to ensure we are running on time
                

