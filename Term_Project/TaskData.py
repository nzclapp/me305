"""
   @file                    FTerm_TaskData.py
   @brief                   This file allows us to collect data from our balance system
   @details                 This file allows us to collect position, velocity, angle, and omega
                            data and save it to a file
   @author                  Nolan Clapp and Parker Tenney
   @date                    December 6, 2021
"""
import utime
import os
from ulab import numpy as np
class TaskData:
    ''' @brief      Data Collection 
        @details    Collects data from the system and allows us to utalize it for graphing
    '''
    def __init__(self, period,Data,xpos,ypos,measuredx,measuredy):
        ''' @brief              Constructs the interface task.
            @details            The interface task is implemented as a finite state
                                machine.
            @param period       The period, in microseconds, between runs of 
                                the task.
            @param Data         Share object that turns the data collection on or off
            @param xpos         The current x position
            @param ypos         The current y position
            @param measuredy    The current x angle
            @param measuredx    The current y angle
            
        '''     
        ## @brief           Shared variable for Period
        #  @details         This variale controls the period at which the program runs
        self.period=period
        ## @brief           Shared variable for Data Collection
        #  @details         This variale allows us to use choose whether or not we want to collect data
        self.Data=Data
        ## @brief           Shared variable for x position on the touch panel
        #  @details         This variale allows us to share the x position read from 
        #                   the touch panel, but only when the panel is being touched
        self.xpos=xpos
        ## @brief           Shared variable for y position on the touch panel
        #  @details         This variale allows us to share the y position read from 
        #                   the touch panel, but only when the panel is being touched
        self.ypos=ypos
        ## @brief           Shared variable for the measured x angle
        #  @details         This variable allows us to share x angle in order to find omega
        self.measuredx=measuredx
        ## @brief           Shared variable for the measured y plane angle
        #  @details         This variable allows us to share y angle in order to find omega.
        #                   find the omega
        self.measuredy=measuredy
        ## @brief           This variable is for the old measured x angle
        #  @details         Allows us to take a difference of the currently read theta and the 
        #                   old theta, to find the omega x
        self.oldthetax = self.measuredx.read()
        ## @brief           This variable is for the old measured y angle
        #  @details         Allows us to take a difference of the currently read theta and the 
        #                   old theta, to find the omega y
        self.oldthetay = self.measuredy.read()
        ## @brief           This variable an array that holds our collection data
        #  @details         An array that holds position, velocity, angle, and omega data
        self.DataList = ()
        ## @brief           This variable is the time the data collection has been running
        #  @details         A variable that is the total time the data has been collecting from the system
        self.Time=0
        ## @brief           This variable sets the intial state to zero
        #  @details         A variable that allows us to have a init state within our data collection
        self.state=1
        ## @brief           This variable sets speed at whcih data collection runs
        #  @details         A variable that sets the frequency that the data collection runs 
        #                   at in order to conserve memory comsumption of data collection
        self.n=0
        ## @brief           This variable is for the old measured x position
        #  @details         Allows us to take a difference of the currently read x position and the 
        #                   old x position, to find the velocity x
        self.xold=0
        ## @brief           This variable is for the old measured y position
        #  @details         Allows us to take a difference of the currently read y position and the 
        #                   old y position, to find the velocity y
        self.yold=0
        ## @brief           This variable sets the file name
        #  @details         Allows us to call the file name throughout the file with self
        self.filename = "Data.txt"
        ## @brief           This variable is the current time as seen from the timer
        #  @details         A variable that allows us to find the time the system has been running by subtracting old time
        self.count=utime.ticks_us()
        
    def run(self):
         
            
            if self.Data.read()==True:  # Checks to see if data collection need to be turned on
                
                if self.state == 1:  # Starts the data collection by clearing the file "Data" of any pervious runs
                    ## @brief           This variable is the old time as seen from the timer
                    #  @details         A variable that allows us to find the time the system has been running by subtracting old time
                    self.oldtime = utime.ticks_ms()
                    self.state+=1
                    with open(self.filename, 'w') as f:
                        f.write(f"0\r\n")
                self.Time= (utime.ticks_ms()-self.oldtime)/(10**3)
                ##print (self.Time)
                if self.Time > self.n:  # Only runs on set time interval self.n
                    
                    # Below is the current positions and angles used to calculate ball velocity and ang velocity
                
                    ## @brief           This variable is for the current measured x position
                    #  @details         Allows us to take a difference of the current x position and the 
                    #                   old x position, to find the velocity x
                    x=self.xpos.read()
                    ## @brief           This variable is for the current measured y position
                    #  @details         Allows us to take a difference of the current y position and the 
                    #                   old x position, to find the velocity y
                    y=self.ypos.read()
                    ## @brief           Variable for the current measured x plane angle
                    #  @details         This variable allows us to measure x angle in order to find omega.
                    #                   find the omega
                    thetax=self.measuredx.read()
                    ## @brief           Variable for the current measured y plane angle
                    #  @details         This variable allows us to measure y angle in order to find omega.
                    #                   find the omega
                    thetay=self.measuredy.read()
                    
                    # Below calculates current velocitys and omegas
                    
                    ## @brief           Variable for the current measured x ball speed
                    #  @details         This variable allows us to measure the current speed of the ball
                    #                   in the x direction
                    self.Vx=(x-self.xold)/((utime.ticks_ms()-self.oldtime)/(10**3))
                    xold=self.xpos.read()
                    
                    ## @brief           Variable for the current measured y ball speed
                    #  @details         This variable allows us to measure the current speed of the ball
                    #                   in the y direction
                    self.Vy=(y-self.yold)/((utime.ticks_ms()-self.oldtime)/(10**3))
                    yold=self.ypos.read()
                    
                    ## @brief           Variable for the current measured x omega.
                    #  @details         This variable allows us to measure the current x omega
                    #                   also known as theta dot
                    omegaX = (self.measuredx.read() - self.oldthetax)/((utime.ticks_ms()-self.oldtime)/(10**3))
                    self.oldthetax = self.measuredx.read()
                    
                    ## @brief           Variable for the current measured y omega.
                    #  @details         This variable allows us to measure the current y omega
                    #                   also known as theta dot
                    omegaY = (self.measuredy.read() - self.oldthetay)/((utime.ticks_ms()-self.oldtime)/(10**3))
                    self.oldthetay = self.measuredy.read()
                    
                    
                    
                    self.DataList += (self.Time,x,y,self.Vx,self.Vy,thetax,thetay,omegaX,omegaY)  # appends the datalist with the most recent data 
                    self.n+=.2
                    
                    if self.Time >=5:  # After 5 seconds of data collection the datalist is written to a text file
                        with open(self.filename, 'w') as f:
                            f.write(f"{self.DataList}\r\n")
                        self.Data.write(False)
                        print(self.DataList)

            
            