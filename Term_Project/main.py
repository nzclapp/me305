
"""
   @file                    FTerm_main.py
   @brief                   This file provides our main code to run eaech task at a certain interval
   @details                 This file is our main code that deals with the shares and queues between files
                            runs out tasks at a set interval
   @author                  Nolan Clapp and Parker Tenney
   @date                    October 18, 2021
"""
import shares
import pyb
import TouchDriver
import TaskTouch
import taskIMU
import MotorDriver
import closedloop
import TaskMotor
import InterfaceTask
import TaskData

def main():
    ''' @brief      The main program
        @details    Runs the main program and creates our shared variables for our other tasks to use 
    '''

    ## @brief           Shared variable for x position on the touch panel
    #  @details         This variale allows us to share the x position read from 
    #                   the touch panel, but only when the panel is being touched
    xpos=shares.Share(0)
    ## @brief           Shared variable for y position on the touch panel
    #  @details         This variale allows us to share the y position read from 
    #                   the touch panel, but only when the panel is being touched
    ypos=shares.Share(0)
    ## @brief           Shared variable for whether or not the panel is being touched
    #  @details         This variale allows us to share whether a touch is being sensed
    #                   by the panel or if there is no touch on the touch pad.
    zpos=shares.Share(False)
    ## @brief           Shared variable for motor duty
    #  @details         This variale allows us to change the motor duty cycle
    duty=shares.Share(0) 
    ## @brief           Shared variable for motor number
    #  @details         This variale allows us to use choose which motor to run
    MotorNum=shares.Share(True)
    ## @brief           Shared variable for the measured x plane angular velocity
    #  @details         This variable allows us to share x plane angular velocity between files
    measuredx=shares.Share(0)
    ## @brief           Shared variable for the measured y plane angular velocity
    #  @details         This variable allows us to share y plane angular velocity between files
    measuredy=shares.Share(0)
    ## @brief           Shared variable for touch calibration
    #  @details         This variale allows us to use choose whether or not we want to calibrate our Touch Panel
    CalibTouch=shares.Share(False)
    ## @brief           Shared variable for IMU calibration
    #  @details         This variale allows us to use choose whether or not we want to calibrate our IMU
    Calib=shares.Share(False)
    ## @brief           Shared variable for Data Collection
    #  @details         This variale allows us to use choose whether or not we want to collect data
    Data=shares.Share(False)
    ## @brief           Shared variable for balancing the ball
    #  @details         This variale allows us to use choose whether or not we want to balance the ball
    Balance=shares.Share(False)
    ## @brief           This shared variable sets the position control gain
    #  @details         Allows us to set and read a shared variable for the gain that 
    #                   adjusts the level of position control.
    km= shares.Share(120)
    ## @brief           This shared variable sets the speed control gain
    #  @details         Allows us to set and read a shared variable for the gain that 
    #                   adjusts the level of speed control.
    kp=shares.Share(240)
    ## @brief           Creates an object for our motor driver
    #  @details         This variable takes in the motor number, and duty
    #                   to choose which motor to run at a certain duty
    MotorObj=MotorDriver.Motor(MotorNum,duty)
    ## @brief           Creates an object for our motor driver
    #  @details         This variable takes in the timer number, for this motor driver
    motordrive = MotorDriver.DRV8847(3)


    # Define all of our tasks below so that they run in the order desired

    ## @brief           Variable for the first task, the user interface
    #  @details         This variable allows the input of period (microsec), IMU calibration,
    #                   position control gain, speed control gain, and data collection.
    task1=InterfaceTask.InterfaceTask(5000, Calib,km,kp,Balance,Data)
    ## @brief           Variable for the second task, the motor control.
    #  @details         This variable allows the input of period (microsec),
    #                   motor object, duty, motor driver, and motor number
    task2=TaskMotor.TaskMotor(5000,MotorObj,motordrive,MotorNum,measuredx,measuredy,xpos,ypos,zpos,Balance,km,kp)
    ## @brief           Variable for the third task, the touch panel
    #  @details         This variable allows the input of period (microsec), x position, y position,
    #                   touch sense, and touch calibration.
    task3 = TaskTouch.TaskTouch(500,xpos,ypos,zpos,CalibTouch)
    ## @brief           Variable for the second task, the IMU
    #  @details         This variable allows the input of period (microsec), x angle,
    #                   y angle, and IMU calibration
    task4 = taskIMU.taskIMU(500,measuredx,measuredy,Calib)    
    ## @brief           Variable for the second task, the data collection
    #  @details         This variable allows the input of period (microsec), data collection,
    #                   x postion, y position, x angle, and y angle.
    task5=TaskData.TaskData(5000, Data,xpos,ypos,measuredx,measuredy)
    
    ## @brief           A list of tasks to run
    TaskList = [task1, task2, task3, task4, task5]


    # This below runs our tasks in a loop, in order
    while(True):
        try:
            for task in TaskList:
                task.run()
            
        except KeyboardInterrupt:
            break
    
    print('Program Terminating')
    #pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)


if __name__=='__main__':
    main()
    # This below runs it only if directly called in putty


