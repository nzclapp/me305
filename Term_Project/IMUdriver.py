"""
   @file                    FTerm_IMUdriver.py
   @brief                   This file is the write up for our ME 305 Term Project
   @author                  Nolan Clapp and Parker Tenney
   @date                    December 8, 2021
"""
import pyb

from pyb import I2C
import struct

## @brief           Variable to set the I2C as master
#  @details         This vaiable allows us to use I2C to calculate our IMU readings
i2c = pyb.I2C(1, pyb.I2C.MASTER)

## @brief           Variable to create a bytearray
#  @details         This creates a bytearray of length 1
buf=bytearray(1)

## @brief           Variable to define the I2C configuration and modes
#  @details         This vaiable allows us choose which configuration and mode we want to run the I2C 
i2c.mem_read(buf, 0x28, 0x00)



class BNO055():
        ''' @brief Interface with IMU
            @details allows us interface with the IMU hardware
        '''    
    
        def __init__(self, euler_vals,measuredx,measuredy):
           ''' @brief Instantiates our IMU so we can take our measurements
               @param euler_vals Shared Tuple that contains the euler angle data
               @param measuredx    Shared veriable for x angle measurment
               @param measuredy    Shared variable for y angle measurment 
           '''
           
           ## @brief           Variable to share the x angle 
           #  @details         This vaiable allows us share the measured x angle to our other tasks
           self.measuredx=measuredx
           ## @brief           Variable to share the y angle 
           #  @details         This vaiable allows us share the measured y angle to our other tasks
           self.measuredy=measuredy
           i2c=pyb.I2C(1,pyb.I2C.MASTER)
           buf=bytearray(4)
           #calbuf=bytearray(22)
           i2c.mem_read(buf, 0x28, 0x00)


        def OPmode(self):
            ''' @brief Defines the pins necessary to set the IMU to operating mode
            '''
            #sets it to op mode
            i2c.mem_write(self.OPR_MODE, 0x28, 0x3D)
            # op mode=    0x0C
            # config mode= 0x00
            
        
        def get_cal_status(self):
            ''' @brief Allows us to calibrate the IMU upon first use
                @details Preform the necessary movements to fully calibrate the IMU and be able to collect data
                 
            '''
            ## @brief           Variable to create a bytearray for the calibration
            cal_bytes=i2c.mem_read(bytearray([4]), 0x28, 0x35)
            
            ## @brief           Variable to show the calibration status
            cal_status = (cal_bytes[0] & 0b11, (cal_bytes[0] & 0b11 << 2) >> 2, (cal_bytes[0] & 0b11 << 4) >> 4, (cal_bytes[0] & 0b11 << 6) >> 6)
            print("Values:", cal_status)
            print('\n')
        def get_cal_coeff(self): 
            ''' @brief Allows us to print the calibration coefficients after the IMU is calibrated
                @details Rather than having to calibrate the IMU each time, after we calibrate, we can store and use the coefficients using this function
                 
            '''
            #code to get the calibration coeffs
            calbuf=bytearray(22)
            i2c.mem_read(calbuf, 0x28, 0x55)
            ## @brief           Variable for the signed integers calculated from calbuf
            cal_signed_int = struct.unpack('<hhhhhhhhhhh', calbuf)
            cal_vals = tuple(cal_int/16 for cal_int in cal_signed_int)
            print('Scaled: ', cal_vals)
            
        
        def set_cal_coeff(self, Cal_Coeff):
            ''' @brief Allows us to set the IMU calibration coeffiecients 
            '''
            #allows us to set the calib coeffs 
            self.i2c.mem_write(Cal_Coeff, 0x28, 0x55)
        
        def read_angles(self):
            ''' @brief Allows us read the euler angles off of the IMU     
                 
            '''
            ## @brief           Variable to hold our byte array of Euler angles
            # @details          Needs to be 6 bytes long because the data comes in MSB and LSB format
            eulbuf=bytearray(6)
            #allows us to read the angles
            i2c.mem_read(eulbuf, 0x28, 0x1A)
            ## @brief           Variable to convert from MSB LSB to a usable value
            eul_signed_int = struct.unpack('<hhh', eulbuf)
            ## @brief           Variable for the tuple of usable Euler Angles
            eul_vals = tuple(eul_int/16 for eul_int in eul_signed_int)
            self.measuredx.write(eul_vals[1])
            self.measuredy.write(eul_vals[2])
            return eul_vals
            
        def read_angularvelo():
            ''' @brief Allows us read the angular velocity off of the IMU     
                 
            '''
            ## @brief           Variable to hold our byte array of angular velocity
            # @details          Needs to be 6 bytes long because the data comes in MSB and LSB format
            angularvelobuf=bytearray(6)
            # allows us to read angular velocity
            i2c.mem_read(angularvelobuf, 0x28, 0x14)
            ## @brief           Variable to convert from MSB LSB to a usable value
            angularvelo_signed_int= struct.unpack('<hhh', angularvelobuf)
            ## @brief           Variable for the tuple of usable angular velocity measurments
            angularvelo_vals=tuple(angularvelo_int/16 for angularvelo_int in angularvelo_signed_int)
            print(angularvelo_vals)
            

#main test code to test the calibration stuff
if __name__== '__main__':
    import IMUdriver
    import time
    c=2
    if c==2:
        status = IMUdriver.get_cal_status()[0]
        print("calibrating...")
        magCali = False
        accCali = False
        gyrCali = False
        while status != 0b11111111:
            if ((status & 0b11) == 0b11) & (magCali == False):
                print("magnetomer calibrated")
                magCali = True
            if ((status & 0b1100) == 0b1100) & (accCali == False):
                print("accelerometer calibrated")
                accCali = True
            if ((status & 0b110000) == 0b110000) & (gyrCali == False):
                print("gyroscope calibrated")
                gyrCali = True
            status = IMUdriver.get_cal_status()[0]
        print("Calibrated")
        
        # print calibration data to file
        calibration = IMUdriver.get_cal_coeff()
        print(calibration)
        caliFile = open('CalibrationCoefficients', 'w')
        caliFile.write(calibration)
        caliFile.close()
    

        print('(heading, pitch, roll) deg')

    while True:
        angles = IMUdriver.read_angles()
        print(angles)
        time.sleep(1)
             