"""
   @file                    FTerm_taskIMU
   @brief                   This file provides our code to interact with the IMU Driver
   @details                 This file contains the code necessary to call the functions in the IMU
                            driver so we can collect the necessary measurements from it
   @author                  Nolan Clapp and Parker Tenney
   @date                    Dec 8, 2021
"""

import IMUdriver
import pyb
import struct
import time


class taskIMU():
    ''' @brief A taskIMU class so we can read data from IMU
    '''
    def __init__(self,period,measuredx,measuredy,Calib):
        '''
        @brief      Initializes the IMU task with required shares
        @details    Allows necessary parameters to be passed into task IMU so we can collect and share data
        @param period   Variable to determine how often this task runs
        @param measuredx    Shared veriable for x angle
        @param measuredy    Shared variable for y angle
        @param Calib        Share to determine when we want to calibrate the IMU
        '''
        ## @brief           Defines the period taskIMU should run at 
        #  @details         Defines the period of the task IMU as the first input to init
        
        self.period=period
        ## @brief           Shared variable for the measured x plane angular velocity
        #  @details         This variable allows us to share x plane angular velocity between files
        self.measuredx=measuredx
        ## @brief           Shared variable for the measured y plane angular velocity
        #  @details         This variable allows us to share y plane angular velocity between files
        self.measuredy=measuredy
        ## @brief           This variable sets the op mode for the IMU driver
        #  @details         This variable sets the op mode using configuration 0x0C
        self.IMU=IMUdriver.BNO055(0x0C,measuredx,measuredy) #sets op mode to 0x0C
        ## @brief           Variable to set the I2C as master
        #  @details         This vaiable allows us to use I2C to calculate our IMU readings
        self.i2c=pyb.I2C(1,pyb.I2C.MASTER)



        #accel= 0x08
        #euler= 0x1A
        #anvel= 0x14
        
        ## @brief           Variable to create a byte array
        #  @details         This vaiable is set to a byte array of length 6
        self.buf=bytearray(6)
        ## @brief           Variable to create a byte array
        #  @details         This vaiable is set to a byte array of length 4
        self.cal_buf = bytearray([4])
        ## @brief           Variable to create a byte array
        #  @details         This vaiable is set to a byte array of length 22
        self.calbuf=bytearray(22)
        ## @brief           Variable to create a byte array
        #  @details         This vaiable is set to a byte array of length 6
        self.angularvelobuf=bytearray(6)
        ## @brief           Variable to define the I2C configuration and modes
        #  @details         This vaiable allows us choose which configuration and mode we want to run the I2C 
        self.i2c.mem_write(0x0C, 0x28, 0x3D)
        ## @brief           Shared variable to choose when to calibrate 
        #  @details         Allows us to choose to calibrate the IMU in task encoder 
        self.Calib=Calib
        
    def run(self):
            ''' @brief              Runs the functions in taskIMU
                @details            This function is responsible for running the functions 
                                    in IMUdriver and collect necessary data
            '''
            # this reads the angles off the imu
            self.IMU.read_angles()
            # this contains the calibration code 
            if self.Calib.read()==True:
                ## @brief           Variable to create a bytearray for the calibration
                cal_bytes=self.i2c.mem_read(self.cal_buf, 0x28, 0x35)
                ## @brief           Variable to create an array for the calibration data
                cal_status = (cal_bytes[0] & 0b11, (cal_bytes[0] & 0b11 << 2) >> 2, (cal_bytes[0] & 0b11 << 4) >> 4, (cal_bytes[0] & 0b11 << 6) >> 6)
                print("Values:", cal_status)
                print('\n')
                
                ## @brief Variable to tell if the accelerometer is calibrated
                accel=False
                ## @brief Variable to tell if the gyroscope is calibrated
                Gyr=False
                ## @brief Variable to tell if the magnetometer is calibrated
                Mag=False
                if cal_status[0]==3:
                    print('Accelerometer Calibrated')
                    accel=True
                if cal_status[1]==3:
                    print('Gyroscope Calibrated')
                    Gyr=True
                if cal_status[2]==3:
                    print('Magnatomoter Calibrated')
                    Mag=True
                # only runs when its fully calibrated    
                if accel==True and Gyr==True and Mag==True:
                
                   ## @brief Variable to collect the byte data from the IMU
                   self.calbuf=bytearray(22)
                   self.i2c.mem_read(self.calbuf, 0x28, 0x55)
                   ## @brief           Variable to convert from MSB LSB to a usable value
                   cal_signed_int = struct.unpack('<hhhhhhhhhhh', self.calbuf)
                   ## @brief           Variable for the tuple of usable Euler Angles
                   cal_vals = tuple(cal_int/16 for cal_int in cal_signed_int)
                   print('Scaled: ', cal_vals)
                   self.Calib.write(False)
                   time.sleep(.25)


            

             