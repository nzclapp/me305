

"""
   @file                    main.py
   @brief                   This file provides our main code to run eaech task at a certain interval
   @details                 \image html Task_FSM.jpg "Task Diagram and Finite State Machine"
   @page                    Lab 0x04 Report  
                            For lab 0x04, we integrated closed loop control into our motor task. This addition allows us to input a desired  angular velocity in RPM and a gain value, KP. Below is a block diagram of our proportional only controller (P controller). This block diagram represents a model that will allow us to perform closed loop feedback control to our motor system to run at a certain RPM value.  
                            \image html BlockDiagram.jpg "Block Diagram for Closed Loop Response"
                            In order to tune our closed loop motor control, we tested several Kp values at the same set point. The progression of our tuning is shown below. We tested a Kp range from 0.1 to 0.5. to determine the optimal gain value.  
                            \image html kp.1.jpg
                            \image html kp.2.jpg
                            \image html kp.3.jpg
                            \image html kp.4.jpg
                            \image html kp.5.jpg
                            \image html kp.55.jpg
                            \image html kp.6.jpg "Motor Response for Kp from .1 to .6"
   @author                  Nolan Clapp and Parker Tenney
   @date                    October 18, 2021
"""
import shares
import TaskEncoder
import InterfaceTask
import EncoderDriver
import MotorDriver
import closedloop
import TaskMotor
import pyb

if __name__ == '__main__':
    
    def main():
        ''' @brief      The main program
            @details    Runs the main program and creates our shared variables for our other tasks to use 
        '''
        
        ## @brief           Shared variable for encoder position 
        #  @details         This variale allows us to determine the encoder position
        EncPos = shares.Share()
        ## @brief           Shared variable for encoder delta 
        #  @details         This variale allows us to determine the encoder delta
        Delta = shares.Share()
        ## @brief           Shared variable for encoder zero 
        #  @details         This variale allows us to determine the encoder zero
        EncZero = shares.Share()
        ## @brief           Shared variable for encoder object 
        #  @details         This variale allows us to set the encoder object to read encoder position
        EncObj=shares.Share(True)
        ## @brief           Shared variable for encoder zero 
        #  @details         This variale allows us to determine the encoder zero
        fault=shares.Share()
        ## @brief           Shared variable for motor duty
        #  @details         This variale allows us to set the motor duty in task 
        #                   user and read it in task motor
        duty=shares.Share(0) 
        ## @brief           Shared variable for motor number
        #  @details         This variale allows us to use choose which motor to run
        MotorNum=shares.Share()
        ## @brief           Shared variable for motor proportional gain
        #  @details         This variale allows us to use choose the proportional gain
        kp=shares.Share(0)
        ## @brief           Shared variable for the desired RPM
        #  @details         This is a shared variable for the RPM
        insignal=shares.Share(0)
        ## @brief           Shared variable for the measured angular velocity
        #  @details         This variable allows us to share angular velocity between files
        measured=shares.Share(0)
        
        ## @brief           This variable allows us to choose closed or open loop control
        #  @details         Allows us to input a duty cycle or desired RPM and Kp value
        CLOL=shares.Share()
        
        
        insignal=shares.Share(100)
        
        ## @brief           Creates an object for our encoder driver 1
        #  @details         This variable takes in the timer number, and pins required to define the encoder
        EncoderDriverObj1=EncoderDriver.EncoderDriver(4, pyb.Pin.cpu.B6, pyb.Pin.cpu.B7)
        ## @brief           Creates an object for our encoder object 2
        #  @details         This variable takes in the timer number, and pins required to define the encoder
        EncoderDriverObj2=EncoderDriver.EncoderDriver(8, pyb.Pin.cpu.C6, pyb.Pin.cpu.C7)
        ## @brief           Creates an object for our motor driver
        #  @details         This variable takes in the motor number, and duty
        #                   to choose which motor to run at a certain duty
        MotorObj=MotorDriver.Motor(MotorNum,duty)
        ## @brief           This creates an object for the closed loop control
        #  @details         This variable creates an object for closed loop control
        #                   so we can input a desired RPM and Kp value
        ClosedLoopObj=closedloop.closedloop(kp,insignal,measured)
        ## @brief           Creates an object for our motor driver
        #  @details         This variable takes in the timer number, for this motor driver
        motordrive = MotorDriver.DRV8847(3)
        ## @brief           Variable for the first task
        #  @details         This variable allows the input of period (microsec), encoder object
        #                   encoder position, delta, and zero
        task1 = TaskEncoder.TaskEncoder(5000, EncoderDriverObj1,EncoderDriverObj2, EncPos, Delta, EncZero,EncObj, measured)
        ## @brief           Variable for the second task
        #  @details         This variable allows the input of period (microsec),
        #                   encoder position, delta, and zero
        task2 = InterfaceTask.InterfaceTask(10000, EncPos, Delta, EncZero,EncObj,fault,duty,MotorNum,kp,insignal,measured,CLOL)
        ## @brief           Variable for the third task (Task Motor)
        #  @details         This variable allows the input of period (microsec),
        #                   motor object, duty, motor driver, and motor number
        task3=TaskMotor.TaskMotor(5000,MotorObj,duty,motordrive,MotorNum,kp,insignal,measured,ClosedLoopObj,CLOL)
    
        
        ## @brief           A list of tasks to run
        TaskList = [task1 , task2, task3]
        
        while(True):
            try:
                for task in TaskList:
                    task.run()
                
            except KeyboardInterrupt:
                break
        
        print('Program Terminating')
        #pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)


   

