
"""
   @file                    Lab3main.py
   @brief                   This file provides our main code to run eaech task at a certain interval
   @details                 \image html Task_FSM.jpg "Task Diagram and Finite State Machine"
   @details                 The graphs below demonstrate that we can reliably collect motor data for 30 seconds for each motor/encoder pair.
   @details                 \image html MotorData.jpg "Motor Position vs Time for Both Motors"
   @author                  Nolan Clapp and Parker Tenney
   @date                    November 9, 2021
"""

import shares
import TaskEncoder
import InterfaceTask
import EncoderDriver
import MotorDriver
import TaskMotor
import pyb

if __name__ == '__main__': 
    def main():
        ''' @brief      The main program
            @details    Runs the main program and creates our shared variables for our other tasks to use 
        '''
        
        ## @brief           Shared variable for encoder position 
        #  @details         This variale allows us to determine the encoder position
        EncPos = shares.Share()
        ## @brief           Shared variable for encoder delta 
        #  @details         This variale allows us to determine the encoder delta
        Delta = shares.Share()
        ## @brief           Shared variable for encoder zero 
        #  @details         This variale allows us to determine the encoder zero
        EncZero = shares.Share()
        
        EncObj=shares.Share()
        ## @brief           Shared variable for encoder zero 
        #  @details         This variale allows us to determine the encoder zero
        fault=shares.Share()
        ## @brief           Shared variable for motor duty
        #  @details         This variale allows us to set the motor duty in task 
        #                   user and read it in task motor
        duty=shares.Share(0) 
        ## @brief           Shared variable for motor number
        #  @details         This variale allows us to use choose which motor to run
        MotorNum=shares.Share()
        
        ## @brief           Creates an object for our encoder object 1
        #  @details         This variable takes in the timer number, and pins required to define the encoder
        EncoderDriverObj1=EncoderDriver.EncoderDriver(4, pyb.Pin.cpu.B6, pyb.Pin.cpu.B7)
        ## @brief           Creates an object for our encoder object 2
        #  @details         This variable takes in the timer number, and pins required to define the encoder
        EncoderDriverObj2=EncoderDriver.EncoderDriver(8, pyb.Pin.cpu.C6, pyb.Pin.cpu.C7)
        
        ## @brief           Creates an object for our motor driver
        #  @details         This variable takes in the motor number, and duty
        #                   to choose which motor to run at a certain duty
        MotorObj=MotorDriver.Motor(MotorNum,duty)
       
        ## @brief           Creates an object for our motor driver
        #  @details         This variable takes in the timer number, for this motor driver
        motordrive = MotorDriver.DRV8847(3)
        ## @brief           Variable for the first task
        #  @details         This variable allows the input of period (microsec), encoder object
        #                   encoder position, delta, and zero
        task1 = TaskEncoder.TaskEncoder(5000, EncoderDriverObj1,EncoderDriverObj2, EncPos, Delta, EncZero,EncObj)
        ## @brief           Variable for the second task
        #  @details         This variable allows the input of period (microsec),
        #                   encoder position, delta, zero, and object
        task2 = InterfaceTask.InterfaceTask(10000, EncPos, Delta, EncZero,EncObj,fault,duty,MotorNum)
        ## @brief           Variable for the third task (Task Motor)
        #  @details         This variable allows the input of period (microsec),
        #                   motor object, duty, motor driver, and motor number
        task3=TaskMotor.TaskMotor(5000,MotorObj,duty,motordrive,MotorNum)
    
        
        ## @brief           A list of tasks to run
        TaskList = [task1 , task2, task3]
        
        while(True):
            try:
                for task in TaskList:
                    task.run()
                
            except KeyboardInterrupt:
                break
        
        print('Program Terminating')
        #pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)
    
    
       
    
