"""
   @file                    TaskEncoder.py
   @brief                   This file provides the code to interface with TaskUser and Encoder Driver
   @author                  Nolan Clapp and Parker Tenney
   @date                    October 18, 2021
"""

import EncoderDriver
import utime 
import pyb
import shares


class TaskEncoder:
    ''' @brief          Interface with the encoder driver
        @details        Inerfaces with the encoder driver in order to call the functions defined 
                        in encoder driver. 
    '''
    def __init__ (self, period, EncoderDriverObj1, EncoderDriverObj2, EncPos, Delta, EncZero, EncObj, measured):
        
        
        ## @brief           This creates an object from encoder driver
        #  @details         This variable creates an object from encoder driver,
        #                   which will allow us to utilize multiple encoders without
        #                   creating multiple files
        self.EncoderDriverObj1=EncoderDriverObj1
        ## @brief           This creates an object from encoder driver
        #  @details         This variable creates an object from encoder driver,
        #                   which will allow us to utilize multiple encoders without
        #                   creating multiple file
        self.EncoderDriverObj2=EncoderDriverObj2
        ## @brief           Shared Variable for Encoder Position
        #  @details         This variable allows us to share the encoder position
        #                   between task user and task encoder
        self.EncPos = EncPos
        ## @brief           Shared Variable for Encoder Delta
        #  @details         This variable allows us to share the encoder delta
        #                   between task user and task encoder
        self.Delta = Delta
        ## @brief           Shared Variable for Encoder Zero
        #  @details         This variable allows us to update the encoder position to
        #                   zero, and verify 
        self.EncZero = EncZero
        ## @brief           Shared Variable for the Encoder Object
        #  @details         This variable allows us to choose which encoder we 
        #                   want to read position from
        self.EncObj = EncObj
        ## @brief           Defines the period of the encoder 
        #  @details         Defines the period of the encoder as the largest 16 bit 
        #                   number, 65,535
        self.period = period
        
        ## @brief           Defines the initial state 
        #  @details         Sets the state zero to a value of zero to use in the FSM
        self.S0_INIT=0
        ## @brief           Defines the first state 
        #  @details         Sets the state to a value of 1 to use in FSM
        self.S1_Update_Zero=1
        ## @brief           Defines the variable for measured RPM
        #  @details         This variable allows us to share the RPM from this task to others
        self.measured = measured
        ## @brief           Varaible for state of FSM
        #  @details         Sets the state of the FSM to state 0
        self.state=self.S0_INIT
        ## @brief           Varaible time in ticks
        #  @details         Sets the variable for time in ticks using the onboard utime
        self.time = utime.ticks_us()
        ## @brief           Varaible for old time
        #  @details         Sets the initial value of time to zero, allows us to calculate velocity
        self.old = 0
        
        
    def run(self):
        ''' @brief              Runs the functions in EncoderDriver
            @details            This function is responsible for running the functions 
                                in Encoder driver including updating encoder and delta, and sharing these
                                values to the user interface task. 
        '''  
        if (self.state==self.S0_INIT):  
            self.count=utime.ticks_us()
            self.state=self.S1_Update_Zero
            
        elif (self.state==self.S1_Update_Zero):
            if self.count>=self.period:
                if self.EncObj.read() == True:
                    #print("yoyoyo")
                    if self.EncZero.read()==True:
                        self.EncoderDriverObj1.update()
                        self.EncoderDriverObj1.set_position(0)
                        self.EncPos.write(self.EncoderDriverObj1.get_position())
                        self.EncZero.write(False)
                    else: 
                        self.EncoderDriverObj1.update()
                        self.EncPos.write(self.EncoderDriverObj1.get_position())
                        self.Delta.write(self.EncoderDriverObj1.get_delta())
                        self.time = utime.ticks_us()
                        self.speed = ((self.Delta.read())*(2*3.14159/4000))/((self.time-self.old)/1000000)
                        #print(self.time-self.old)
                        self.old = self.time
                        self.rpm = self.speed*9.5493
                        self.measured.write(self.rpm)
                    self.count=utime.ticks_add(self.count,self.period*-1)
                    
                    
                elif self.EncObj.read() == False:
                    
                    if self.EncZero.read()==True:
                        self.EncoderDriverObj2.update()
                        self.EncoderDriverObj2.set_position(0)
                        self.EncPos.write(self.EncoderDriverObj2.get_position())
                        self.EncZero.write(False)
                    else: 
                        self.EncoderDriverObj2.update()
                        self.EncPos.write(self.EncoderDriverObj2.get_position())
                        self.Delta.write(self.EncoderDriverObj2.get_delta())
                        self.time = utime.ticks_us()
                        print(self.time-self.old)
                        self.speed = ((self.Delta.read())*(2*3.14159/4000))/((self.time-self.old)/1000000)
                        self.old = self.time
                        self.rpm = self.speed*9.5493
                        self.measured.write(self.rpm)
                    self.count=utime.ticks_add(self.count,self.period*-1)
                    
            
        else:
            raise(ValueError('This is Invalid State Value'))
      
    
    def transition_to(self, new_state):
        ''' @brief          Transistions from one state to another 
            @details        This function is responsible for transitioning from one state 
                            another by calling transition_to("desired state")
        '''  
        self.state=new_state
        