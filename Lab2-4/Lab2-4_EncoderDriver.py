"""
   @file                    EncoderDriver.py
   @brief                   This file provides the code to enterface directly with the encoder hardware
   @author                  Nolan Clapp and Parker Tenney
   @date                    October 18, 2021
"""

import pyb


class EncoderDriver:
    ''' @brief Interface with quadrature encoders
        @details
    '''
        
    def __init__(self,TimerNumber,Ch1,Ch2):
        ''' @brief Constructs an encoder object
            
        '''
        ## @brief           Sets up the on board timer on the Nucleo
        #  @details         This variable allows us to use Timer4 on the Nucleo with a period
        #                   of the largest 16 bit number with a prescaler of 0
        self.tim8 = pyb.Timer(TimerNumber, prescaler=0, period=65535)
        
        ## @brief           Defines the location of the encoder timer channel on the Nucleo
        #  @details         Defines the location of the encoder timer channel on the Nucleo
        #                   as channel 1
        self.ch1= self.tim8.channel(1, pyb.Timer.ENC_AB, pin=Ch1)
        
        ## @brief           Defines the location of the encoder timer channel on the Nucleo
        #  @details         Defines the location of the encoder timer channel on the Nucleo
        #                   as channel 2
        self.ch2= self.tim8.channel(2, pyb.Timer.ENC_AB, pin=Ch2)

        ## @brief           Defines the period of the encoder 
        #  @details         Defines the period of the encoder as the largest 16 bit 
        #                   number, 65,535
        self.period=65535
        
        ## @brief           Keeps track of the previous count from the encoder timer
        #  @details         Defines the variable responsible for keeping track of the 
        #                   previous encoder count, used in calculating delta
        self.countprev=0
        
        ## @brief           Defines the curent encoder position
        #  @details         This variable keeps track of the current encoder position
        self.EncoderPos = 0
        

        
        print('Creating encoder object')
        
    def update(self):
        ''' @brief Updates encoder position and delta
            @details This function is responsible for updating the encoder position
            and delta values, at a constant interval defined in the main program file. 
        '''      
        #print('Reading encoder count and updating position and delta values')
        #current count- previous count
        
        ## @brief           Creates a variable that keeps track of the count
        #  @details         This variable uses the onboard coutner to determine
        #                   encoder position
        count=self.tim8.counter()
        self.delta=count-self.countprev
        
        #check delta and fix
        if self.delta>self.period/2:
            self.delta-=self.period

        elif self.delta< -self.period/2:
            self.delta+=self.period

        
        #add delta to position variable to 
        self.EncoderPos+=self.delta
        #make previous count = current count 
        self.countprev=count
        
        return self.EncoderPos
    
    
    def get_position(self):
        ''' @brief Returns encoder position
            @details Returns the current encoder positoin, calculated in update
            @return The position of the encoder shaft
        '''
        return self.EncoderPos
    
    def set_position(self, position):
        ''' @brief Sets encoder position
            @details sets the current encoder position to an integer value
            @param position The new position of the encoder shaft
        '''
       
        self.EncoderPos=position
        
    
    def get_delta(self):
        ''' @brief Returns encoder delta
            @details Returns the encoder delta value, determined in the update function
            @return The change in position of the encoder shaft
            between the two most recent updates
        '''
        return self.delta