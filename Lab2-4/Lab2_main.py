# -*- coding: utf-8 -*-
"""
   @file                    main2.py
   @brief                   This file provides our main code to run eaech task at a certain interval
   @details                 \image html Task_FSM.jpg "Task Diagram and Finite State Machine"
   @author                  Nolan Clapp and Parker Tenney
   @date                    October 18, 2021
"""

import shares
import TaskEncoder
import InterfaceTask
import EncoderDriver
import pyb

def main():
    ''' @brief      The main program
        @details    Runs the main program and creates our shared variables for our other tasks to use 
    '''
    ## @brief           Shared variable for encoder position 
    #  @details         This variale allows us to determine the encoder position
    EncPos = shares.Share()
    ## @brief           Shared variable for encoder delta 
    #  @details         This variale allows us to determine the encoder delta
    Delta = shares.Share()
    ## @brief           Shared variable for encoder zero 
    #  @details         This variale allows us to determine the encoder zero
    EncZero = shares.Share()
    
    ## @brief           Creates an object for our encoder driver
    #  @details         This variable takes in the timer number, and pins required to define the encoder
    EncoderDriverObj1=EncoderDriver.EncoderDriver(4, pyb.Pin.cpu.B6, pyb.Pin.cpu.B7)
    
    ## @brief           Variable for the first task
    #  @details         This variable allows the input of period (microsec), encoder object
    #                   encoder position, delta, and zero
    task1 = TaskEncoder.TaskEncoder(5000, EncoderDriverObj1, EncPos, Delta, EncZero)
    ## @brief           Variable for the second task
    #  @details         This variable allows the input of period (microsec),
    #                   encoder position, delta, and zero
    task2 = InterfaceTask.InterfaceTask(10000, EncPos, Delta, EncZero)
    
    ## A list of tasks to run
    TaskList = [task1 , task2]
    
    while(True):
        try:
            for task in TaskList:
                task.run()
            
        except KeyboardInterrupt:
            break
    
    print('Program Terminating')
    #pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)

if __name__ == '__main__':
    main()

