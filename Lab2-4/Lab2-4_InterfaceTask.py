"""
   @file                    InterfaceTask.py
   @brief                   This file provides our main code to interact with the user inputs
   @author                  Nolan Clapp and Parker Tenney
   @date                    October 18, 2021
"""
import pyb
import utime
import time



class InterfaceTask:
    ''' @brief      Interface with the user
        @details    Interfaces with the user to gather keyboard inputs that
                    control the program
    '''
    
    def __init__(self, period, EncPos, Delta, EncZero, EncObj, fault, duty, MotorNum, kp, insignal, measured, CLOL):
        ''' @brief              Constructs the interface task.
            @details            The interface task is implemented as a finite state
                                machine.
            @param period       The period, in microseconds, between runs of 
                                the task.
            @param EncPos       The position of the encoder
            @param Delta        The delta of the encoder position
            @param EncZero      Share object that is used to zero the encoder
        '''     
        ## @brief           The initial state of the FSM
        #  @details         This variable allows us to set the initial state to 0
        self.S0_INIT = 0
        ## @brief           The first state of the FSM
        #  @details         This variable allows us to set the state to 1
        self.S1_WAIT_FOR_INPUT = 1
        ## @brief           The second state of the FSM
        #  @details         This variable allows us to set the state to 2
        self.S2_ZERO_POS = 2
        ## @brief           The third state of the FSM
        #  @details         This variable allows us to set the state to 3
        self.S3_PRINT_ENC_POS = 3
        ## @brief           The fourth state of the FSM
        #  @details         This variable allows us to set the state to 4
        self.S4_PRINTS_DELTA = 4
        ## @brief           The fifth state of the FSM
        #  @details         This variable allows us to set the state to 5
        self.S5_COLLECTS_DATA = 5
        ## @brief           The sixth state of the FSM
        #  @details         This variable allows us to set the state to 6
        self.S6_ENDS_COLLECTION = 6
        ## @brief           The seventh state of the FSM
        #  @details         This variable allows us to set the state to 7
        self.S7_FAULT_CLEAR = 7
        ## @brief           The eigth state of the FSM
        #  @details         This variable allows us to set the state to 8
        self.S8_SPEED_CONTROL = 8
        ## @brief           The ninth state of the FSM
        #  @details         This variable allows us to set the state to 9
        self.S9_PRINT_SPEED = 9
        ## @brief           The tenth state of the FSM
        #  @details         This variable allows us to set the state to 10
        self.S10_CLOSED_LOOP = 10
        ## @brief           The first state of the first alternae FSM
        #  @details         This variable allows us to set the alternate state 
        #                   to 1 inside of speed control
        self.input_state = 1
        ## @brief           The first state of the second alternae FSM
        #  @details         This variable allows us to set the alternate state 
        #                   to 1 inside of speed control
        self.input_state2 = 1


        ## @brief           Defines the period of the encoder 
        #  @details         Defines the period of the encoder as the largest 16 bit 
        #                   number, 65,535
        self.period = period
        ## @brief           Shared Variable for Encoder Position
        #  @details         This variable allows us to share the encoder position
        self.EncPos = EncPos
        ## @brief           Shared Variable for Encoder Delta
        #  @details         This variable allows us to share the encoder delta
        #                   between task user and task encoder
        self.Delta = Delta
        ## @brief           Shared Variable for Encoder Zero
        #  @details         This variable allows us to update the encoder position to
        #                   zero, and verify
        self.EncZero = EncZero
        ## @brief           Shared Variable for Encoder CheckEncoderPosition
        #  @details         This variable allows us to check when this is true,
        #                   We use this to identify which encoder to control
        self.EncObj = EncObj
        ## @brief           Shared Variable for motor control 
        #  @details         This variable allows us to share which encoder we
        #                   are controlling.     
        self.MotorNum = MotorNum
        ## @brief           Shared Variable for motor fault
        #  @details         This variable allows us to share if the motor faults  
        self.fault = fault
        ## @brief           Shared Variable for motor duty
        #  @details         This variable allows us to share what value 
        #                   of duty we desire
        self.duty=duty
        ## @brief           Shared Variable for closed loop gain
        #  @details         This variable allows us to share what value 
        #                   of gain we desire for closed loop control
        self.kp=kp
        ## @brief           Shared Variable for the insignal rpm
        #  @details         This variable allows us to share what the insignal 
        #                   rpm value is
        self.insignal = insignal
        ## @brief           Shared Variable for the measured rpm
        #  @details         This variable allows us to share what the measured 
        #                   rpm value is
        self.measured=measured
        ## @brief           Shared Variable for the closed loop control
        #  @details         This variable allows us to share whether we want  
        #                   open or closed loop control
        self.CLOL=CLOL
        ## @brief           Defines speed_str as a string
        #  @details         This variable allows us to use speed_str as a string
        #                   in order to float it into a numeric value
        self.speed_str = ""
        ## @brief           Defines speed_str2 as a string
        #  @details         This variable allows us to use speed_str2 as a string
        #                   in order to float it into a numeric value
        self.speed_str2 = ""
        ## @brief           Defines kp_str as a string
        #  @details         This variable allows us to use kp_str as a string
        #                   in order to float it into a numeric value
        self.kp_str = ""
        ## @brief           Defines a time variable for next time
        #  @details         This variable allows us to compare time to next time,
        #                   and fall into the FSM
        self.NextTime = utime.ticks_add(utime.ticks_us(), self.period)
        ## @brief           Sets the initial state to zero
        #  @details         This variable allows us to move into the zero state of the FSM
        self.state = self.S0_INIT
        ## @brief           Interface with the keyboard
        #  @details         Allows us to take keyboard inputs, and use them in our
        #                   code as values.
        self.ser_port = pyb.USB_VCP()
        
        
        
    def run(self):
        ''' @brief          Runs the user inteface task.
            @details        The interface task is implemented as a finite state
                            machine.
        '''     
        ## @brief           Counts the time
        #  @details         Variable that counts the total time elapsed
        self.time = utime.ticks_us()
        
        if (utime.ticks_diff(self.time, self.NextTime) >= 0):
                if self.state == self.S0_INIT:
                    print('Press these buttons below to use our software:')
                    print('z:       Zero the position of encoder 1')
                    print('Z:       Zero the position of encoder 2')
                    print('p:       Print out the position of encoder 1')
                    print('P:       Print out the position of encoder 2')
                    print('d:       Print out the delta for encoder 1')
                    print('D:       Print out the delta for encoder 2')
                    print('g:       Collect encoder 1 position for 30 seconds')
                    print('G:       Collect encoder 2 position for 30 seconds')
                    print('s or S:  End data collection prematurely')
                    print('c or C:  Clear a fault condition of motor')
                    print('w:       Print out the speed of encoder 1')
                    print('W:       Print out the speed of encoder 1')
                    print('l:       Closed Loop control of motor 1')
                    print('L:       Closed Loop control of motor 2')
                    print('o:       Open Loop Control of motor 1')
                    print('O:       Open Loop Control of motor 2')
                    
                    self.state = self.S1_WAIT_FOR_INPUT
                    
                if self.state == self.S1_WAIT_FOR_INPUT:
                    
                    if(self.EncZero.read() == True):
                        self.EncZero.write(False)
                    
                    if self.ser_port.any():
                        ## @brief           Variable that is the user input
                        #  @details         Variable that is defined as the keyboard stroke
                        #                   pressed by the user
                        self.userinput = self.ser_port.read(1)
                        
                        if(self.userinput == b'z'):
                            self.enc = 1
                            self.EncObj.write(True)
                            self.state = self.S2_ZERO_POS
                            
                        elif(self.userinput == b'Z'):
                            self.enc = 2
                            self.EncObj.write(False)
                            self.state = self.S2_ZERO_POS
                            
                            
                        elif(self.userinput == b'p'):
                            self.enc = 1
                            self.EncObj.write(True)
                            self.state = self.S3_PRINT_ENC_POS
                            
                            
                        elif(self.userinput == b'P'):
                            self.enc = 2
                            self.EncObj.write(False)
                            self.state = self.S3_PRINT_ENC_POS
                            
                             
                        elif(self.userinput == b'd'):
                            self.enc = 1
                            self.EncObj.write(True)
                            self.state = self.S4_PRINTS_DELTA
                            
                        elif(self.userinput == b'D'):
                            self.enc = 2
                            self.EncObj.write(False)
                            self.state = self.S4_PRINTS_DELTA
                            
                        elif(self.userinput == b'g'):
                            self.enc = 1
                            self.EncObj.write(True)
                            self.state = self.S5_COLLECTS_DATA
                            ## @brief       Makes a variable for the current time
                            #  @details     Variable that is defined as the current time
                            #               when g is pressed
                            self.oldtime = self.time
        
                        elif(self.userinput == b'G'):
                            self.enc = 2
                            print ('Collecting encoder {:} position for 30 seconds' .format(self.enc))
                            self.EncObj.write(False)
                            #set shared enconder object to 2
                            self.state = self.S5_COLLECTS_DATA
                            self.oldtime = self.time
                            
                        elif(self.userinput == b'c' or self.userinput == b'C'):
                            self.state = self.S7_FAULT_CLEAR
                        
                        
                            
                        elif(self.userinput == b'w'):
                            self.enc = 1
                            self.EncObj.write(True)
                            self.oldtime = self.time
                            self.state = self.S9_PRINT_SPEED
                            
                            
                        elif(self.userinput == b'W'):
                            self.enc = 2
                            self.EncObj.write(False)
                            self.oldtime = self.time
                            self.state = self.S9_PRINT_SPEED
                            
                        elif(self.userinput == b'l'):
                            self.CLOL.write(True)     
                            print('enter desired rpm for motor 1')
                            self.enc = 1
                            self.MotorNum.write(True)
                            self.EncObj.write(True)
                            self.input_state = 1
                            self.state = self.S10_CLOSED_LOOP
                            
                        
                        
                        elif(self.userinput == b'L'):
                            self.CLOL.write(True)
                            print('enter desired rpm for motor 2')
                            self.enc = 2 
                            self.MotorNum.write(False)
                            self.EncObj.write(False)
                            self.input_state = 1
                            self.state = self.S10_CLOSED_LOOP
                            
                            
                        elif self.userinput == b'o':
                            self.CLOL.write(False)
                            print('enter desired duty for motor 1')
                            self.enc = 1
                            self.MotorNum.write(True)
                            self.EncObj.write(True)
                            self.input_state = 1
                            self.state = self.S8_SPEED_CONTROL
                            
                            
                        elif self.userinput == b'O':
                            self.CLOL.write(False)
                            print('enter desired duty for motor 2')
                            self.enc = 2 
                            self.MotorNum.write(False)
                            self.EncObj.write(False)
                            self.input_state = 1
                            self.state = self.S8_SPEED_CONTROL
                        
                                        
                    
                elif self.state == self.S2_ZERO_POS:
                    print ('Zeroing the position of encoder {:}' .format(self.enc))
                    self.EncZero.write(True)
                    self.state = self.S1_WAIT_FOR_INPUT
                    
                elif self.state == self.S3_PRINT_ENC_POS:
                    print('Encoder {:}'.format(self.enc), 'position: {:}'.format(self.EncPos.read()))
                    self.state = self.S1_WAIT_FOR_INPUT
                    
                elif self.state == self.S4_PRINTS_DELTA:
                    print ('Encoder {:}'.format(self.enc), 'delta: {:}'.format(self.Delta.read()))
                    self.state = self.S1_WAIT_FOR_INPUT
                    
                elif self.state == self.S5_COLLECTS_DATA:
                    ## @brief       Makes a variable for the new time passed
                    #  @details     Variable that is defined as the time passed
                    #               after g is pressed
                    
                    
                    self.newtime=self.time- self.oldtime

                    if self.newtime<=30000000:
                            try:
                                if self.ser_port.any():
                                    self.userinput = self.ser_port.read(1)
                                    if(self.userinput == b's' or b'S'): 
                                        self.state = self.S6_ENDS_COLLECTION
                                    else: 
                                        self.state = self.S5_COLLECTS_DATA
                                else:
                                    self.newtime = self.time - self.oldtime
                                    print('{:.3f}, {:}'.format(self.newtime/1000000, self.EncPos.read()))
                                    time.sleep(.02)
                            except KeyboardInterrupt:
                                self.state = self.S6_ENDS_COLLECTION                        
                    else:
                        print('Data collecton finished, it is given in the format of [time, encoder {:} position]'.format(self.enc))
                        self.state = self.S1_WAIT_FOR_INPUT
                                 

                elif self.state == self.S6_ENDS_COLLECTION:
                    print('Data collecton finished, it is given in the format of [time, encoder {:} position]'.format(self.enc))
                    self.state = self.S1_WAIT_FOR_INPUT
                
                elif self.state == self.S7_FAULT_CLEAR:
                    self.fault.write(True)
                    self.duty=0
                    self.MotorNum.write(False)
                    self.MotorNum.write(True)
                    
                    print('Clearing fault condition')
                    self.state=self.S1_WAIT_FOR_INPUT
                elif self.state == self.S8_SPEED_CONTROL:
                    
                    
                    if self.input_state == 1:
                        
                        if self.ser_port.any():
                            ## @brief           Variable that is the user input
                            #  @details         Variable that is defined as the keyboard stroke
                            #                   pressed by the user
                            self.char_in = self.ser_port.read(1).decode()
                            
                            if self.char_in.isdigit():
                                self.speed_str += self.char_in
                                print(self.speed_str)
                                self.input_state = 1
                                
                            elif self.char_in == '-':
                                if self.speed_str == "":
                                    self.speed_str += self.char_in
                                    print(self.speed_str)
                                    self.input_state = 1
                                else:
                                    self.input_state = 1
                            elif self.char_in =='.':
                                if "." in self.speed_str:
                                    self.input_state = 1
                                else:
                                    self.speed_str += self.char_in
                                    print(self.speed_str)
                            elif self.char_in =='\x7F':
                                #self.speed_str = self.speed_str.rstrip(self.speed_str[-1])
                                self.speed_str = self.speed_str[:-1]
                                print(self.speed_str)
                                
                            elif self.char_in == '\r' or self.char_in == '\n':
                                self.input_state = 2
                                print('stinky')

                    elif self.input_state == 2:
                        ## @brief       Makes a variable for a floated speed string
                        #  @details     Variable that is defined as the numeric value
                        #               for speed floated from speed_str
                        self.speed_number = float(self.speed_str)
                        print(self.speed_number)
                        self.duty.write(self.speed_number)
                        self.state = self.S1_WAIT_FOR_INPUT
                                
                
                elif self.state == self.S9_PRINT_SPEED:
                    ## @brief       Makes a variable for the new time passed
                    #  @details     Variable that is defined as the time passed
                    #               after g is pressed
                    #print(self.measured.read())
                    #self.state = self.S1_WAIT_FOR_INPUT
                    
                    self.newtime= self.time - self.oldtime
                    

                    if self.newtime<=30000000:
                            try:
                                if self.ser_port.any():
                                    self.userinput = self.ser_port.read(1)
                                    if(self.userinput == b's' or b'S'): 
                                        self.state = self.S6_ENDS_COLLECTION
                                    else: 
                                        self.state = self.S5_COLLECTS_DATA
                                else:
                                    self.newtime = self.time - self.oldtime
                                    print('{:.3f}, {:}'.format(self.newtime/1000000, self.measured.read()))
                                    
                            except KeyboardInterrupt:
                                self.state = self.S6_ENDS_COLLECTION                        
                    else:
                        print('Data collecton finished, it is given in the format of [time, encoder {:} speed(rpm)]'.format(self.enc))
                        self.state = self.S1_WAIT_FOR_INPUT
                    
                    
                    
                    #self.time = utime.ticks_us()
                    #self.speed = ((self.Delta.read())*(2*3.14159/4000))/((self.time-self.old)/1000000)
                    #self.old = self.time
                    #self.rpm = self.speed*9.5493
                    #print(self.rpm)
                    #self.state = self.S1_WAIT_FOR_INPUT
                    #print(self.measured.read())
                    
                    
                
                
                elif self.state == self.S10_CLOSED_LOOP:
                    
                                
                    if self.input_state2 == 1:
                        

                            ## @brief           Variable that is the user input
                            #  @details         Variable that is defined as the keyboard stroke
                            #                   pressed by the user
                        
                        if self.ser_port.any():
                            
                            self.char_in2 = self.ser_port.read(1).decode()
                            
                            if self.char_in2.isdigit():
                                self.speed_str2 += self.char_in2
                                print(self.speed_str2)
                                self.input_state2 = 1
                                
                            elif self.char_in2 == '-':
                                if self.speed_str2 == "":
                                    self.speed_str2 += self.char_in2
                                    print(self.speed_str2)
                                    self.input_state2 = 1
                                else:
                                    self.input_state2 = 1
                            elif self.char_in2 =='.':
                                if "." in self.speed_str2:
                                    self.input_state2 = 1
                                else:
                                    self.speed_str2 += self.char_in2
                                    print(self.speed_str2)
                            elif self.char_in2 =='\x7F':
                                self.speed_str2 = self.speed_str2[:-1]
                                print(self.speed_str2)
                                    
                            elif self.char_in2 == '\r' or self.char_in2 == '\n':
                                self.input_state2 = 2
                                
                                
                    elif self.input_state2 == 2:
                        self.speed_number2 = float(self.speed_str2)
                        print(self.speed_number2)
                        self.insignal.write(self.speed_number2)
                        self.input_state2 = 3
                        print('Enter Value for Kp')
                                
                    elif self.input_state2 == 3:
                        
                        if self.ser_port.any():
                            self.char_in3 = self.ser_port.read(1).decode()
                                
                            if self.char_in3.isdigit():
                                self.kp_str += self.char_in3
                                print(self.kp_str)
                                self.input_state2 = 3
                                
                            elif self.char_in3 == '-':
                                if self.kp_str == "":
                                    self.kp_str += self.char_in3
                                    print(self.kp_str)
                                    self.input_state2 = 3
                                else:
                                    self.input_state2 = 3
                            elif self.char_in3 =='.':
                                if "." in self.kp_str:
                                    self.input_state2 = 3
                                else:
                                    self.kp_str += self.char_in3
                                    print(self.kp_str)
                                    
                            elif self.char_in3 =='\x7F':
                            
                                self.kp_str = self.kp_str[:-1]
                                print(self.kp_str)
                                    
                            elif self.char_in3 == '\r' or self.char_in3 == '\n':
                                print('lessgo')
                                self.input_state2 = 4
                                    
                    elif self.input_state2 == 4:
                        ## @brief       Makes a variable for a floated gain string
                        #  @details     Variable that is defined as the numeric value
                        #               for gain floated from kp_str
                        self.kp_number = float(self.kp_str)
                        print(self.kp_number)
                        self.kp.write(self.kp_number)
                        self.oldtime= self.time
                        self.state = self.S1_WAIT_FOR_INPUT
                        

                    
        
                else:
                    raise ValueError('Invalid State')