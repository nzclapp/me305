"""
   @file                    closedloop.py
   @brief                   This file provides the code to creates a closed loop motor model
   @author                  Nolan Clapp and Parker Tenney
   @date                    November 15, 2021
"""
class closedloop:
    
    def __init__(self,kp,insignal,measured):
        ## @brief           This creates an variable for proportional gain
        #  @details         This variable creates an object from encoder driver,
        #                   which will allow us to utilize multiple encoders without
        self.kp=kp
        ## @brief           Variable for the desired RPM
        #  @details         This is a shared variable for the RPM
        self.insignal=insignal
        ## @brief           Variable for the measured angular velocity
        #  @details         This variable allows us to share angular velocity between files
        self.measured=measured
        print('Creating P Controller')
        
    
    def run(self):
        ''' @brief              Runs the functions in closedloop
            @details            This function is responsible for running the functions 
                                in closedloop task
            @return             Returns the value for a, the product of the error signal and Kp
        '''
        ## @brief           Variable for error signal
        #  @details         This variable allows us to calculate the closed loop error signal so we can do closed loop control of our motors
        e=self.insignal.read()-self.measured.read()
        self.a=e*self.kp.read()        
        return self.a
    
    def get_kp(self):
        ''' @brief              Allows us to print the current Kp value            
            @return             Returns the current Kp value
        '''
        return self.kp.read()
    
    def set_kp(self,PorportionalGain):
        ''' @brief              Allows us to write the Kp value 
            @param              Input the desired Proportional gain           
            
        '''
        self.kp.write(PorportionalGain)
    
    
