# -*- coding: utf-8 -*-
"""
Created on Sun Oct 24 15:49:54 2021

@author: nclap
"""
import utime
import MotorDriver

class TaskMotor:
    
    def __init__(self,period,MotorObj,duty,motordrive, MotorNum,kp,insignal,measured,ClosedLoopObj,CLOL):
        ## @brief           Defines the period of the encoder 
        #  @details         Defines the period of the encoder as the largest 16 bit 
        #                   number, 65,535
        self.period=period
        
        ## @brief           Defines the count used to run the task
        #  @details         Uses onboard Utime timer to create a count to be 
        #                   sure our task is running on time
        self.count=utime.ticks_us()
        ## @brief           Allows us to choose which motor to run
        #  @details         We can input the motor number (1 or 2) for the motor 
        #                   we want to run
        self.MotorNum = MotorNum
        ## @brief           This creates an object from motor driver for each motor
        #  @details         This variable creates an object from the motor driver,
        #                   which will allow us to utilize multiple motors without
        #                   creating multiple files
        self.MotorObj=MotorObj
        ## @brief           Defines the duty cycle for the motor
        #  @details         This variable allows us to input a percent (0-100)
        #                   for the duty cycle we want the motor to run at
        self.duty=duty
        ## @brief           Creates an object for our motor driver
        #  @details         This variable takes in the timer number, to fully
        #                   define our motor driver
        self.motordrive=motordrive
        ## @brief           Enables the motor driver
        #  @details         This variable enables the motor driver so that we can 
        #                   actually run the motor for a given duty cycle
        self.motordrive.enable()
        ## @brief           This creates an variable for proportional gain
        #  @details         This variable creates an object from encoder driver,
        #                   which will allow us to utilize multiple encoders without
        ## @brief           Variable for the desired RPM
        #  @details         This is a shared variable for the RPM
        self.insignal=insignal
        ## @brief           Variable for the measured angular velocity
        #  @details         This variable allows us to share angular velocity between files
        self.measured=measured
        ## @brief           Variable for the current time
        #  @details         This variable allows us to count using the onboard timer
        self.count=utime.ticks_us()
        ## @brief           This creates an object for the closed loop control
        #  @details         This variable creates an object for closed loop control
        #                   so we can input a desired RPM and Kp value
        self.ClosedLoopObj=ClosedLoopObj
        ## @brief           This variable sets the state to 0
        #  @details         Allows us to transition to state 0
        self.S0_Init=0
        ## @brief           This variable sets the state to 1
        #  @details         Allows us to transition to state 1
        self.S1_InputState=1
        ## @brief           This variable sets the state to 2
        #  @details         Allows us to transition to state 2
        self.S2_RunCL=2
        ## @brief           This variable sets the state to 3
        #  @details         Allows us to transition to state 3
        self.S3_RunOL=3
        ## @brief           This variable sets the state variable to 0
        #  @details         Allows us to set the state as an integer
        self.state=0
        ## @brief           This variable allows us to choose closed or open loop control
        #  @details         Allows us to input a duty cycle or desired RPM and Kp value
        self.CLOL=CLOL
        ## @brief           This variable sets initial a value to zero
        #  @details         Allows calculate error*kp to produce the desuired closed loop response
        self.a=0
        
    def run(self):
        ''' @brief              Runs the functions in TaskMotor
            @details            This function is responsible for running the functions 
                                in Motor Task and actually allows the motors to run
        '''
        if self.count>=self.period:
            self.count+=self.period
            
            if self.state==0:
                
                self.state=1
            
            
            elif self.state==1:
                
                if (self.CLOL.read()==True):
                    
                    #self.insignal=self.insignal.read()
                    #self.measured=self.measured.read()
                    #self.kp = self.kp.read()
                    self.state=2
                    
                elif (self.CLOL.read()==False):
                    
                    self.state=3
                    
            elif self.state==2:
                self.insignal1 = self.insignal.read()
                
                self.measured1 = self.measured.read()
                #print(self.measured1)
                e = self.insignal1- self.measured1
                self.a=e*self.kp.read()  
                self.newduty = self.a
                
                #self.a=self.ClosedLoopObj.run()
                self.MotorObj.set_duty(self.newduty)
                
                
            elif self.state==3:
                self.MotorObj.set_duty(self.duty.read())

        