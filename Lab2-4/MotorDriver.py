# -*- coding: utf-8 -*-
"""
Created on Tue Oct 19 13:31:16 2021

@author: nclap
"""

''' @file DRV8847.py'''

import pyb
import time
import utime
import shares


class DRV8847:
    ''' @brief A motor driver class for the DRV8847 from TI.
        @details Objects of this class can be used to configure the DRV8847
                motor driver and to create one or moreobjects of the
                Motor class which can be used to perform motor
                control.
                Refer to the DRV8847 datasheet here: https://www.ti.com/lit/ds/symlink/drv8847.pdf
    '''

    def __init__ (self,TimerNumber):
        ''' @brief Initializes and returns a DRV8847 object.
        '''   
        ## @brief           Creates a variable for the timer number
        #  @details         This variable takes in the timer number and freq. to create a timer
        self.tim3 = pyb.Timer(TimerNumber, freq=20000)
        
        ## @brief           Creates a variable for motor sleep pin
        #  @details         This variable defines pinA15 as the pin to toggle motor sleep
        self.pinA15=pyb.Pin(pyb.Pin.cpu.A15)
        
        ## @brief           Creates a variable for motor fault pin
        #  @details         This variable defines pinB2 as the pin to toggle motor fault
        self.pinB2=pyb.Pin(pyb.Pin.cpu.B2)

        ## @brief           Edits a variable for motor sleep pin
        #  @details         This variable sets pinA15 as the pin to set motor sleep as an output
        self.pinA15 = pyb.Pin (pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
        
        self.pinA15.low()
        
        ## @brief           Creates a variable for motor fault interrupt
        #  @details         This variable allows us to recognize and deal with motor faults
        self.motorInt = pyb.ExtInt(self.pinB2, mode=pyb.ExtInt.IRQ_FALLING,pull=pyb.Pin.PULL_NONE, callback= self.fault_cb)
    
    def enable (self):
        ''' @brief Brings the DRV8847 out of sleep mode.
        '''
        print('Enabled')
        self.motorInt.disable()                # Disable fault interrupt
        self.pinA15.high()                      # Re-enable the motor driver
        utime.sleep_us(25)                      # Wait for the fault pin to return high
        self.motorInt.enable()                 # Re-enable the fault interrupt
        
    def disable (self):
        ''' @brief Puts the DRV8847 in sleep mode.
        '''
        self.pinA15.low()
        
        
    def fault_cb (self, IRQ_src):
        ''' @brief Callback function to run on fault condition.
            @param IRQ_src The source of the interrupt request.
        '''
        print('Fault Detect')
        self.disable()
        
        
        
        
    def motor (self,MotorChannel,Ch1Pin,Ch2Pin):
        ''' @brief Initializes and returns a motor object associated with the DRV8847.
            @return An object of class Motor
        '''
        return Motor(MotorChannel,Ch1Pin,Ch2Pin,self.tim3)
    

    
class Motor:
    ''' @brief A motor class for one channel of the DRV8847.
        @details Objects of this class can be used to apply PWM to a given
        DC motor.
    '''
    def __init__ (self, MotorNum,duty):
        ''' @brief Initializes and returns a motor object associated with the DRV8847.
            @details Objects of this class should not be instantiated
            directly. Instead create a DRV8847 object and use
            that to create Motor objects using the method
            DRV8847.motor().
        '''
        
        ## @brief           Defines the duty cycle for the motor
        #  @details         This variable allows us to input a percent (0-100)
        #                   for the duty cycle we want the motor to run at
        self.duty=duty
        
        ## @brief           Creates a variable for motor1 Ch1 pin        
        self.pinB4=pyb.Pin(pyb.Pin.cpu.B4)
        ## @brief           Creates a variable for motor1 Ch2 pin   
        self.pinB5=pyb.Pin(pyb.Pin.cpu.B5)
        ## @brief           Creates a variable for motor2 Ch1 pin   
        self.pinB0=pyb.Pin(pyb.Pin.cpu.B0)
        ## @brief           Creates a variable for motor2 Ch2 pin   
        self.pinB1=pyb.Pin(pyb.Pin.cpu.B1)
        
        self.tim3=pyb.Timer(3, freq=20000)
        self.MotorNum=MotorNum
        self.MotorNum.write(True)
       
            
    def set_duty (self,duty):
        ''' @brief Set the PWM duty cycle for the motor channel.
            4
            @details This method sets the duty cycle to be sent
            to the motor to the given level. Positive values
            cause effort in one direction, negative values
            in the opposite direction.
            @param duty A signed number holding the duty
            cycle of the PWM signal sent to the motor
        '''
        #print(duty)
        self.tim3 = pyb.Timer(3, freq = 20000)
        
        if (self.MotorNum.read()==True):
            self.tim3chA= self.tim3.channel(1, pyb.Timer.PWM, pin=self.pinB4)
            self.tim3chB= self.tim3.channel(2, pyb.Timer.PWM, pin=self.pinB5)

            
              
        elif (self.MotorNum.read()==False):
            self.tim3chA= self.tim3.channel(3, pyb.Timer.PWM, pin=self.pinB0)
            self.tim3chB= self.tim3.channel(4, pyb.Timer.PWM, pin=self.pinB1)

           
        if (duty>=0):   
        
            self.tim3chA.pulse_width_percent(duty)
            self.tim3chB.pulse_width_percent(0)
            
        elif (duty<0):
            self.tim3chA.pulse_width_percent(0)
            self.tim3chB.pulse_width_percent(-1*duty)
                      
            
      

        

        
        

                
        
            
        

     