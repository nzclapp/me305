# -*- coding: utf-8 -*-
"""
Created on Thu Sep 23 11:23:26 2021

@author: nclap
"""

def fib(idx):
    if idx < 0:                                                                 # Check for negative numbers                                                                                  
            return 'not able to be determined due to invalid input, please try again'
    a=0
    b=1                                                                         # First two terms of fib seq
    for n in range(idx):                                                        # Counts up from 0 to the index input           
        a,b = b, a+b                                                            # Bottom up approach where a is set to b, then b is the sum of a+b, and then a=a+b meaning b=a+b+b, and the loop continues until idx is reached
    return a 
    
        
if __name__== '__main__':                                                       # Prevents code from being run if imported as a module
    while True:
        
        idx = input('To continue, press enter, or type "q" to quit.   ')        # This is the start of the loop that is displayed each time the loop is run
        if idx =="q":                                                           # Looking for "q" input, or else it moves into next loop looking for a number
                print('Program Quit')                                           # Confirms that the program was quit properly
                break  
        else:            
                try:
                    idx = int(input('Please Enter Index Number:     '))         # Looks for an intiger input from idx once the origional prompt is answered
                
                except ValueError:                                              # Makes sure that the input is a number
                    print("Invalid Input, try another")                         # If the input is not an intiger, the loop is started over
                    continue
        
                else:
                    print('The Fibonacci number is {:}'.format(fib(idx)))       # Displays "the fib number is"... and the correct fib number associated with the index                 
                    continue