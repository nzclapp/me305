# -*- coding: utf-8 -*-
"""
Created on Tue Sep 21 12:54:05 2021

@author: nclap
"""

def fibseq(idx):
    

    if idx < 0:
            print('Input Cannot Be Negative')
            return 'not able to be determined due to invalid input, please try again'
    elif idx==0:
        return 0    
    elif idx==1:
        return 1  
    else:
        return(fibseq(idx-1)+fibseq(idx-2))
        
      
if __name__== '__main__':       
    while True:
        
        idx = input('Continue, Y or N?   ')
        if idx =="N":
                print('Program Quit')
                break 
        elif idx==int():
                print('Invalid Entry, Try Again')
                break
        else:
                    
                try:
                    idx = int(input('Please Enter Index Number:     '))
                
                except ValueError:
                    print("Invalid Input, try another")
                    continue
                else:
                    print('The Fibonacci number is {:}'.format(fibseq(idx)))                        
                    continue